# UA People & Groups

## API Reference

#### EDS

URL: https://3ol4qo7d2b.execute-api.us-west-2.amazonaws.com/prod

- [List](#EDS-List)
- [Person](#EDS-Person)
- [People](#EDS-People)
- [Search](#EDS-Search)

#### Authentication

Authenticate by providing an API key in the `X-API-KEY` header.

#### Parameters

API endpoints accept HTTP requests with JSON bodies where applicable.

#### Responses

Successful requests to endpoints will yield responses with `Content-Type: application/json` and status code `200`, unless otherwise specified.

---

### EDS List

Accepts the name of an EDS attribute and a list of values corresponding to that attribute. Returns a list of people who match the query.

> Note: The term 'id' is misleading here; `ids` and `ids_type` are the name and values of any EDS attribute, respectively.

#### Endpoint

`GET` `/eds/list`

#### Body

- `ids_type` `string` **Required**

  An EDS attribute name to match the `ids` values against.

- `ids` `string[]` **Required**

  A list of values for the EDS attribute `ids_type`.

- `attributes` `string[]`

  The EDS attributes to return for each person. Fewer attributes speeds up the return. If not provided, all EDS attributes for each person are returned.

---

### EDS Person

Accepts a UA Person identifier and returns all populated EDS attributes, i.e., only those with values, of the person.

#### Endpoint

`GET` `/eds/person`

#### Query String

- `uid`

  A UA Person identifier. One of uid (netid), emplid, uaid, mail, isonumber

---

### EDS People

Returns a set of attributes depending on `ua_person_type` for each person in the query.

#### Endpoint

`GET` `/eds/people`

#### Query String

- `ua_person_type` **Required**

  Must be one of: `employee` `dcc` `student` `student-employee` `employee-student` `retiree` `affiliate`

- `iso`

  If provided, the iso value must be one of: `true` (has an iso number), `false` (does not have an iso number). Not specified returns all.

- `status`

  If provided, the status value must be one of: `active`, `inactive`. Not specified returns all.

---

### EDS Search

Queries EDS where `cn=first_name*last_name*`. The first_name and last_name must be at least 3 characters. Returns the EDS data for each user in the list, if found.

#### Endpoint

`GET` `/eds/search`

#### Headers

- `Eds-Attributes` - A JSON array - `string[]`

  The EDS attributes to return for each person. Fewer attributes speeds up the return. If not provided, all EDS attributes for each person are returned.

#### Query String

- `first_name` **Required**

  At least 3 letters of the beginning of a UA Person's first name. Wildcards are not allowed.

- `last_name` **Required**

  At least 3 letters in a UA Person's last name. Wildcards are not allowed.

---
