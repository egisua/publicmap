# Campus Map Developer Reference

## Options

* Building repository
* Layer controls
* Zoom levels
* Coordinates


### Building repository
****

The Building feed is a crude JSON file at the moment, and can be found here:  

[http://maps.arizona.edu/public/json/buildings.json](http://)

It's not the best, but it is usable, and contains the following useful data:

* Building ID
* Building Name

#### Highlighting a building

Use the 'id' parameter to set the building

Here's how you would show the Administration Building:

`http://maps.arizona.edu/public/?x=-12350876&y=3793798&lod=18&id=355`


### Layer controls
****


There are 10 layers to choose from, and here is an example of how to use them:

Use the `toc` parameter

The link would look something like this if you want to show the parking layer:

`http://maps.arizona.edu/public/?x=-12351010&y=3793171&lod=19&toc=rbtParking` 

#### List of all options


| Layer                       | Parameter Value   |
|----------------------------:|:------------------|
| Accessibility               | rbtAccessibility  |
| Athletics                   | rbtAthletics      |
| Mueseums & Art Venues       | rbtMuseums        |
| Health & Safety             | rbtSafety         |
| Libraries                   | rbtLibraries      |
| Parking                     | rbtParking        |
| Places to Eat               | rbtEateries       |
| Student Housing             | rbtHousing        |
| Transportation              | rbtTransportation |
| Attractions & Landmarks     | rbtAttractions    |


### Zoom Levels
****

The campus map has a few zoom levels, but I don't remember what they are

Use the `lod` parameter to set the zoom level


### Coordinates

You can set the map center coordinates like so `x=-12350876&y=3793798`

Here's how you would show the Administration Building at those coordinates for example:

`http://maps.arizona.edu/public/?x=-12350876&y=3793798&lod=18&id=355`

