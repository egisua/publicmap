dojo.require("dijit.TooltipDialog");
dojo.require("esri.virtualearth.VETiledLayer");
dojo.require("esri.layers.FeatureLayer");
dojo.require("esri.layers.GraphicsLayer");
dojo.require("esri.tasks.find");
dojo.require("esri.tasks.FindTask");
dojo.require("esri.Color");
dojo.require("esri.tasks.query");
dojo.require("esri.tasks.QueryTask");
dojo.require("esri.dijit.Popup");
dojo.require("esri.symbols.SimpleFillSymbol");
dojo.require("esri.symbols.SimpleLineSymbol");
dojo.require("esri.tasks.IdentifyTask");
dojo.require("esri.tasks.IdentifyParameters");
//dojo.require("esri.SpatialReference");
dojo.require("esri.arcgis.utils");
dojo.require("esri.dijit.Print");
dojo.require("esri.dijit.InfoWindowLite");
dojo.require("dijit.Dialog");
dojo.require("esri.symbols.TextSymbol");
dojo.require("esri.symbols.Font");
dojo.require("esri.graphic");
dojo.require("esri.map");
dojo.require("dojo._base.lang");

//esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol

var map;
var url;
var bldgInfoContent;
var constBoundaryInfoContent;
var queryTask, query;
var searchResults;
var roomPolygons;
var ajaxGPSRequest;
var identifyTask, identifyParams;
var projectsDialog;
var getGPSFeed = false;
var resbuildingFlag = 0;
//var routeIds=[8001996,8002000,8002004,8002008,8002012,8002016,8002024];
var routeIds=[8004294,8004210,8004212,8004218,8004214,8002016,8004220];
var qsHasID = false;
var deferred;
var interiorServiceURL = "https://services.maps.arizona.edu/pdc/rest/services/Interior/MapServer/";; 
var interiorLayers = ['14','15','16','17','18','19','20','21','22','23','24','25','26','27']
var customFL;
var polygonSymbol;
var font;
var Textcolor;
var saveLog = false;
var tour;
var loading;
var selectedGraphics;
var restEndpoint;

function HelpView(){
    var view = new ReloadView($('#tourReload'));
  //console.log($('#tourReload'));
  var steps = [{
    content: '<div class="tour-brand"></div>Welcome to the<br><strong>University of Arizona<br> Interactive Map</strong>',
    highlightTarget: false,
    nextButton: true,
    closeButton: true,
    target: $('#tourstart'),
    my: 'right bottom',
    at: 'left center'
  },  {
    content: '<h2>Reset Map</h2><p>You can reset the map to the initial state by clicking the <i class="ico-reload"></i> icon</p>',
    highlightTarget: true,
    closeButton: true,
    nextButton: true,
    target: $('#tourReload'),
    my: 'left center',
    at: 'right center',
    setup: function(tour, options){
     $('#tourReload').bind('click.stepTwo',function(){
      tour.next();
    })
   },
   teardown: function(tour, options){
     $('#tourReload').unbind('click.stepTwo');
     $('#tab2').click()
   },
 },
 
{
  content: '<h2>Search Box</h2><p>Type building name into the search box.</p><p><b>Note: </b>Building number, alias or abbreviation will work equally.</p>',
  highlightTarget: true,
  nextButton: true,
  closeButton: true,
  target: $('#BuildingDD'),
  my: 'right center',
  at: 'left center',
  /* 
  // Section below disbles auto selection of next help item upon text box click
  setup: function(tour, options){
  
   $('#BuildingDD').bind('click.stepThree', function(event){
      tour.next();
    })
 },
 */
 teardown: function(tour, options){
   $('#BuildingDD').unbind('click.stepThree');
   $('#tab1').click();
 },
},

{
  content: '<h2>INTERACTIVE LAYERS</h2><p>Click on the parking layer to move to the next step in the tour.</p><p>You will then see parking lots appear on the  map.</p>',
  highlightTarget: true,
  closeButton: true,
  target: $('#tour2'),
  my: 'right center',
  at: 'left center',
  setup: function(tour, options){
   $('#tour2').bind('click.stepFour',function(){
    setTimeout(function(){tour.next()},900);
    
  })
 },
 teardown: function(tour, options){
   $('#tour2').unbind('click.stepFour');
 },
},{
  content: '<h2>Layer Controls</h2><p>A key will drop down to show related information which you are able to turn on and off.</p><p>Try clicking the word "bicycle" to the right of the <span class="checkbox-status w-icon ico-ua-bicycle-parking"></span>icon above</p>',
  highlightTarget: true,
  nextButton: true,
  closeButton: true,
  target: $('#tour3'),
  my: 'top center',
  at: 'bottom center',
  setup: function(tour, options){
   $('#tour3').bind('click.stepFive',function(){
    tour.next();
  })
 },
 teardown: function(tour, options){
   $('#tour3').unbind('click.stepFive');
 },
},{
  content: '<h2>ZOOM CONTROL</h2><p>You can zoom out to see this building in relation to the rest of campus.</p>',
  highlightTarget: true,
  nextButton: true,
  closeButton: true,
  target: $('.esriSimpleSliderDecrementButton'),
  my: 'left center',
  at: 'right center',
  setup: function(tour, options){
   $('.esriSimpleSliderDecrementButton').bind('click.stepSix',function(){
    tour.next();
  })
 },
 teardown: function(tour, options){
   $('.esriSimpleSliderDecrementButtontour4').unbind('click.stepSix');
 },
},{
  content: '<h2>Share</h2><p>You can then share the current state of the map by clicking the &nbsp;<span class="ico-ua_social"></span>&nbsp; icon</p>',
  highlightTarget: true,
  nextButton: true,
  closeButton: true,
  target: $('#link'),
  my: 'right bottom',
  at: 'left bottom'
}
 ,{
  content: '<h2>More Resources</h2><p>Click "MORE RESOURCES" to reveal links to other maps and directions"</p>',
  highlightTarget: true,
  nextButton: true,
  closeButton: true,
  target: $('#tour5'),
  my: 'right center',
  at: 'left center',
  setup: function(tour, options){
    $('#tour5').focus().bind('change.stepThree', function(event){
      tour.next();
    })
  },
  teardown: function(tour, options){
   $('#tour5').focus().unbind('change.stepThree');
 }
}
]

  tour = new Tourist.Tour({
    steps: steps,
    tipClass: 'Bootstrap',
    tipOptions:{ showEffect: 'slidein' }
  });

  $('.tour-link').click(function(){ tour.start(); });
}

function ReloadView(el){
    var self = this;
    var reload = el.find('i');
  
    reload.click(function(e){
      if(!el.hasClass('disabled'))
        self.select($(e.target))
    });
  
    this.disable = function(){
      el.addClass('disabled');
    }
    this.enable = function(){
      el.removeClass('disabled');
    }
    this.reset = function(){
      el.removeClass('has-selection');
      el.find('a').removeClass('selected');
    }
    this.select = function(reload){
      reload.removeClass('selected')
      reload.addClass('selected')
      el.addClass('has-selection')
      this.trigger('select', this, reload);
    }
    this.getSelected = function(){
      return el.find('.selected');
    }
  }
  _.extend(ReloadView.prototype, Backbone.Events)


  // determine the configured REST endpoint for GIS services from the config file
function getRestEndpointFromConfig() {
    var restEndpoint;
    dojo.xhrGet({
        url: "config.txt",
        handleAs: "text",
        sync: true,
        handle: function (restEndpoints, args) {
            var endpointsArray = restEndpoints.split("\n");
            for (var i=0; i < endpointsArray.length; i++) {
                var url = endpointsArray[i];
                if (url.substring(0, 2) != "//") {
                    restEndpoint = url;
                    break;
                }
            }
        }
    });
    return restEndpoint;
  }

function map_init() {
     // A function is called to read the REST endpoint of the services specified in config.txt file
     restEndpoint = getRestEndpointFromConfig();

    // Setup the popup window
      var popup = new esri.dijit.Popup({
          fillSymbol: new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0, 165, 214]), 2), new dojo.Color([1, 197, 255, 0.9]))          
      }, dojo.create("div", { id: "popover"}));
      /**/
    loading = document.getElementById("loadingImg");  //loading image. id

    map = new esri.Map("map", {
        center: [-110.9505, 32.2319],
        zoom: 17,
		maxZoom: 20,
        smartNavigation: false,
        infoWindow: popup,
        logo: false
    });
	//esri.addProxyRule({proxyUrl:"/proxy/proxy.ashx",urlPrefix:"http://services.maps.arizona.edu"});
    //on(map, "update-start", showLoading);
    //on(map, "update-end", hideLoading);
    
    map.on("update-start", showLoading);
    map.on("update-end", hideLoading);
    
    function showLoading() {
        //console.log("show loadeing")
        esri.show(loading);
        //map.disableMapNavigation();
        //map.hideZoomSlider();
      }
    
      function hideLoading(error) {
          //console.log("hide loadeing")
          esri.hide(loading);
          //map.enableMapNavigation();
          //map.showZoomSlider();
      }

    var imagery = new esri.layers.ArcGISTiledMapServiceLayer("https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer");
        imagery.id = "imagery";
        imagery.visible = false;
        dojo.connect(imagery, "onError", reportError);
        //map.addLayer(imagery);

    var ortho_tile = new esri.layers.ArcGISTiledMapServiceLayer(restEndpoint+"Ortho_19/ImageServer");    
        ortho_tile.id = "ortho_tile";
        ortho_tile.visible = false;
        dojo.connect(ortho_tile, "onError", reportError);
        //map.addLayer(ortho_tile);

    var bing = new esri.virtualearth.VETiledLayer({
        id: "bing",
        bingMapsKey: 'AtE7K3QCsGklmUmkSvTkaP7IcFEBTfXUThpgp8PZhIJ2JugT8k9nC9TbihYL2fuV',
        //OLD ESRI BING KEY...
        //bingMapsKey: 'Al_odDeNeFrAQz9Z840Q1WepmpUI5ZbsRC7IWI2r7M18vuhevz5vZ7wxJwaL41tq',
        mapStyle: esri.virtualearth.VETiledLayer.MAP_STYLE_ROAD,
        //maxScale: 6771
        //maxScale: 1024
        maxScale: 0
    });
    dojo.connect(bing, "onError", reportError);
    //map.addLayer(bing);


    var enterprise_tile = new esri.layers.ArcGISTiledMapServiceLayer(restEndpoint+"publicTiles/MapServer");
    enterprise_tile.id = "enterprise_tile";
    dojo.connect(enterprise_tile, "onError", reportError);
    //map.addLayer(enterprise_tile);
	
    var publicTilesPBC = new esri.layers.ArcGISTiledMapServiceLayer(restEndpoint+"publicTilesPBC/MapServer");
	publicTilesPBC.id = "campusTilesPBC";
    dojo.connect(publicTilesPBC, "onError", reportError);
    //map.addLayer(publicTilesPBC);

    var publicTilesCAC = new esri.layers.ArcGISTiledMapServiceLayer(restEndpoint+"publicTilesCAC/MapServer");
	publicTilesCAC.id = "publicTilesCAC";
    dojo.connect(publicTilesCAC, "onError", reportError);
    //map.addLayer(publicTilesCAC);

    var publicTilesB2 = new esri.layers.ArcGISTiledMapServiceLayer(restEndpoint+"publicTilesB2/MapServer");	
    publicTilesB2.id = "publicTilesB2";
    dojo.connect(publicTilesB2, "onError", reportError);
    //map.addLayer(publicTilesB2);

    var publicTilesTechPark = new esri.layers.ArcGISTiledMapServiceLayer(restEndpoint+"publicTilesTechPark/MapServer");	
    publicTilesTechPark.id = "publicTilesTechPark";
    dojo.connect(publicTilesTechPark, "onError", reportError);
    //map.addLayer(publicTilesTechPark);

    var publicTilesUASouth = new esri.layers.ArcGISTiledMapServiceLayer(restEndpoint+"publicTilesUASouth/MapServer");	
    publicTilesUASouth.id = "publicTilesUASouth";
    dojo.connect(publicTilesUASouth, "onError", reportError);
    //map.addLayer(publicTilesUASouth);

    map.addLayers([imagery, ortho_tile, bing, enterprise_tile, publicTilesPBC, publicTilesCAC, publicTilesB2, publicTilesTechPark, publicTilesUASouth]);

    //url = "https://services.maps.arizona.edu/pdc/rest/services/publicDynamic/MapServer/"
    url = restEndpoint+"publicDynamic/MapServer/";
    identifyTask = new esri.tasks.IdentifyTask(url);

    identifyParams = new esri.tasks.IdentifyParameters();
    identifyParams.tolerance = 3;
    identifyParams.returnGeometry = true;
    identifyParams.layerIds = [29, 51];
    

    identifyParams.layerOption = esri.tasks.IdentifyParameters.LAYER_OPTION_ALL;
    //identifyParams.width = map.width;
    //identifyParams.height = map.height;


    dojo.connect(map, "onClick", dojo.hitch(executeQueryTask));
    dojo.connect(map, "onLoad", initOperationalLayer);
    dojo.connect(map, 'onLoad', function(theMap) {
        //resize the map when the browser resizes
        dojo.connect(dijit.byId('map'), 'resize', map,map.resize);
    });

    /*dojo.connect(map, "onZoomEnd", function(extent, zoomFactor, anchor, level) {
        dimTOCCheckboxes();
        // console.log("checking level: " + level);
        // if(level <= 17) {
        //     console.log("checking center" + extent.xmin);
        //     if (extent.xmin < -12351966) {
        //         console.log("turning on bing");
        //         bing.show();
        //     }
        // }
    });*/    

    font = new esri.symbol.Font();  
    font.setSize("8pt");  
    //font.setWeight(esri.symbol.Font.WEIGHT_BOLD);  

    Textcolor = new esri.Color();  
    Textcolor.setColor([0, 0, 0]); 

    
    dojo.connect(map,"onLoad",function(){
        var initExtent = map.extent;
        dojo.create("i",{
          id: "tourReload",
          className: "ico-reload esriSimpleSliderReload",
          onclick: function(){
            map.setExtent(initExtent);
            $(".layer-controls-wrapper").removeClass("on");
            $(".more-info").removeClass("more-info");
            $(".radio").prop('checked', false);
            $(".button-tag").removeClass("checked");
            $('#searchbox').find('option:first-child').prop('selected', true).end().trigger('liszt:updated');
            searchResults.clear();            
            map.infoWindow.hide();
            console.log('add toc layer...1');
            AddLayersTOC([],function(res){
                

            });
            $('#chkImagery').prop('checked', false).trigger('change');
            $('*').removeClass('o');
          }
        },dojo.query(".esriSimpleSliderDecrementButton")[0], "after");
        // $('.esriSimpleSliderRoload')[0].setAttribute("data-ico", "&#xe000;");

        dojo.create("i",{
            className: "fs1 ico-ua_satellite esriSimpleSliderReload",
            onclick: function(){
              if($("#chkImagery").is(':checked')) {
                  $('#chkImagery').prop('checked', false).trigger('change');
              } else {
                  $("#chkImagery").prop('checked', true).trigger('change');
              }
          }},dojo.query(".esriSimpleSliderReload")[0], "after");        
        
        dojo.create("i",{
            className: "fs1 ico-ua-360 esriSimpleSliderReload",
            onclick: function(){
                if($("#chkAerial360").is(':checked')) {
                    //console.log("aerial 360 checked");
                    $('#chkAerial360').prop('checked', false).trigger('change');
                } else {
                    //console.log("aerial 360 un checked");
                    $("#chkAerial360").prop('checked', true).trigger('change');
                }
        }},dojo.query(".esriSimpleSliderReload")[1], "after");  

        //created();
        HelpView();
    });    
    
    //check querystring parameters to change map extent
    if($.urlParam('x') != null && $.urlParam('y') != null && $.urlParam('lod') != null) {        
        map.centerAndZoom(new esri.geometry.Point($.urlParam('x'), $.urlParam('y'), new esri.SpatialReference({ wkid: 102100 })), $.urlParam('lod'));
    }
    if($.urlParam('id') != null) {
        qsHasID = true;
    }
    if($.urlParam('toc') != null) {
        window.onload = function() {            
            var toc_val = $.urlParam('toc');            
            $('#'+toc_val).trigger("click");            
        };
    } else {
        if($.urlParam('id') != null) {     
            addEGISISFeature($.urlParam('id')); 
        }
    }

    var pathArray = window.location.pathname.split('/');
    let roombldg = pathArray[2];
    if(pathArray[2].split('-').length == 2) {
    let bldg = pathArray[2].split('-')[0];
    let rm = pathArray[2].split('-')[1];
    //console.log(rm, bldg);
    doSearchById(selectedBuildingAlpha);
    loadInterior('', rm, bldg);
    
    } else if (pathArray[2].split('-').length == 1 && pathArray[2].split('-')[0].length > 0) {

    let bldg = pathArray[2].split('-')[0];        
    doSearchById(bldg);
    }  
    
    //$("input[type='radio']").click(function (){  
    $("input[name='layer']").click(function (){  
        //console.log("radio clicked...!");
        changeLayers(this);
      });
    /*
    $("input[name='tabset']").click(function (){  
        console.log("radio clicked...!"+this);
        changeLayers(this);
    });
    */

   $("#btnClearFP").click(function(){
       clearGraphics();
       $("#BuildingDD").val("");
       $("#roominput").val("");

       console.log(window.location);
        //console.log("selectedValue:"+selectedValue+ "- room:"+room+ "- selectedBuildingAlpha:"+selectedBuildingAlpha)
        //loadInterior(selectedValue, room, selectedBuildingAlpha);
        let newURL = window.location.origin+'/'+window.location.pathname.split('/')[1]; //beta server
        window.history.pushState("object or string", "Page Title", newURL);
   });

    //TODO: check querystring parameters to open the TopTOC.
    //use the AddLayersTOC(id) where id = "accessibilityTOC" or "athletics". see switch statement in ua-toc-layers

    //TODO: check querystring parameters to load TopTOC Layers.
    //parse querystring by key:[layerIds]. By default, the layers will all turn on. We want to have ids of which layers to turn off.
    //could be keyed hiddenLayerIds
    //$(".o > input")[0].click();
    //$(".o > input")[3].click();
    //$(".o > input")[5].click();

    //bldgInfoContent = "<img src='https://maps.arizona.edu/photos/buildings/small/${SpaceNum}.jpg' onerror='missingBuildingPhoto(this);' height='120' width='179'>"
    bldgInfoContent = "<img src='https://maps.arizona.edu/photos/buildings/small/${SpaceNumLetter}.jpg' onerror='missingBuildingPhoto(this);' height='138' width='207'>"    
                        + "<div class='bldg-num'> AKA: ${AliasName} </div>"
                        + "<div class='bldg-num'> Address: ${Address} </div>"
                        + "<div class='bldg-num'> Building Number: ${SpaceNumLetter} </div>";
}

function orientationChanged() {
    if(map){
        map.reposition();
        map.resize();
    }
}

formatRouteServed = function (fieldval, fieldname) {
    //console.log(fieldname + ", " + fieldval)
    //console.log(fieldval);
    var routelist = fieldval.split("<BR>");
    if (routelist.length > 0) {
        //console.log(routelist.length + ",Route List has BR:" + routelist);
        var formattedRoutes = "<br>";
        for (var i = 0; i < routelist.length; i++) {
            //console.log(i + "." + routelist[i]);
            formattedRoutes += "&nbsp; &nbsp; &nbsp;- " + routelist[i]+"<br>";
        }
        //formattedRoutes+= "</ul>";
        console.log(formattedRoutes);
    } else {
        //console.log("Route List has no BR:" + routelist)
        var routesArray = routesServed.split(',');
        //console.log("total routes: " + routesArray.length);
    }
    return formattedRoutes;
}

function initOperationalLayer(map) {
    //MapTips Labels
    //  Buildings
    //  Construction
    //  UASitePoints
    //  GreekHouses
    //  athleticsOffCampus
    //  AED 1
    //  AED 2
    //  AED 3
    //  Eateries 1
    //  Eateries 2
    //  WalkingPaths
    //  Fitness Course
    //  Fitness Stops 1
    //  Fitness Stops 2
    //  Fitness Stops 3
    //  OffCampus Landmarks

    //InfoWindows
    //  Buildings
    //  GreekHouses
    //  Audible Classrooms

/*
    //Building Info Window information
    bldgInfoContent = "<img src='https://maps.arizona.edu/photos/buildings/${SpaceNum}.jpg' onerror='missingBuildingPhoto(this);'>"
                + "<div class='bldg-num'> AKA: ${AliasName} </div>"
                + "<div class='bldg-num'> Address: ${Address} </div>"
                + "<div class='bldg-num'> Building Number: ${SpaceNumLetter} </div>";

    var bldgInfoTemplate = new esri.InfoTemplate("<b>${Name}</b>", bldgInfoContent);

    constBoundaryInfoContent = "<div class='bldg-num'> PDC ProjectNumber : ${PDCProjectNumber } </div>"
                + "<div class='bldg-num'> Date Added: ${date_added} </div>";
                //+ "<div class='bldg-num'> Building Number: ${SpaceNumLetter} </div>";
   var constInfoTemplate = new esri.InfoTemplate("<b>${ProjectName}</b>", constBoundaryInfoContent);


    var shuttleStopsContent = "Serving routes :<br>"
                +"USA Downtown: ${RouteIsUSA}<br>"
                +"North South Route: ${RouteIsPurple}<br>"
                +"Southwest Off Campus UA Mall Route: ${RouteIsMauve}<br>"
                +"Inner Campus Route: ${RouteIsTeal}<br>"
                +"Mountain Ave Route: ${RouteIsOrange}<br>"
                +"Outer Campus Loop: ${RouteIsGreen}<br>"
                +"Night Cat: ${RouteIsNight}<br><br>"
                +"<a href='http://parking.arizona.edu/alternative/documents/Cat_tran_brochure_13-14.pdf'>Download Schedule</a>"
    ;

    shuttleStopsContent = "Serving routes :<br>"; */
    //if ("${RouteIsUSA}" == "2")
   //shuttleStopsContent += "USA Downtown: ${RouteIsUSA}<br>";
   
 
 
    //  var shuttleStopsContent = "<p class='infoPara'> Routes Served: ${RoutesServed:formatRouteServed}				<br/>";
	//  shuttleStopsContent += "<a style='color: hotpink;'  href='https://parking.arizona.edu/campus-services/cattran/' target='_blank'>Route Schedules</a> </p>";


    //var shuttleStopsInfoTemplate = new esri.InfoTemplate("<b>${TimedStopName} CatTran Stop</b>", shuttleStopsContent);


    var greekInfoContent = "" //"<img src='http://maps.arizona.edu/WebBuildingPhotos/${Buildings.SpaceNum}.jpg'>"
                + "<div class='bldg-num'> Address: ${Address} </div>"
                //+ "<div class='bldg-num'> Type:${GreekType}</div>";

    var greekInfoTemplate = new esri.InfoTemplate("<b>${Name}</b>", greekInfoContent);

    //Audible Device Classrooms information
    var audibleInfoContent = "<div class='bldg-num'> Building: ${building_name} </div>"
                + "<div class='bldg-num'> Room #: ${ALSRoom_1} </div>"
                + "<div class='bldg-num'><a href='${url}'>More Info</div>";

    var audibleInfoTemplate = new esri.InfoTemplate("<b>Audible Device Classroom</b>", audibleInfoContent);
     
    /*
     var infoWindowLite = new esri.dijit.InfoWindowLite(null, dojo.create("div", null, map.root));
            infoWindowLite.startup();
            //map.setInfoWindow(infoWindowLite);
            //comment if you want the search graphic to disappear after closing the info window.
            dojo.connect(map.infoWindow, "onHide", function() {
                $('#searchbox').find('option:first-child').prop('selected', true).end().trigger('liszt:updated');
                //map.getLayer()
                searchResults.clear();                               
            });
    */

    //map.infoWindow.resize(200, 270);

    //maptip or tooltip constructor
    var dialog = new dijit.TooltipDialog({
        id: "tooltipDialog"
      , style: "position: absolute; text-align:center; width: 125px; font: normal normal bold 10pt Helvetica; z-index:100"
    });
    dialog.startup();

    /** */

    map.graphics.on("mouse-out", function(){
        map.graphics.clear();
        //console.log("mouse out..");
       dijit.popup.close(dialog);
    });
    
    //var buildingsurl = "http://services.maps.arizona.edu/pdc/rest/services/Buildings/MapServer/"
    //var url = "http://pdc-gisdeimos.catnet.arizona.edu:6080/arcgis/rest/services/publicDynamic/MapServer/";
    //var url = "http://pdc-gisphobos.catnet.arizona.edu:6080/arcgis/rest/services/publicDynamic/MapServer/";
    var opLayer;
    var imageParameters = new esri.layers.ImageParameters();
    imageParameters.format = "png32";

    //******************* BUILD QUERY TASK *******************************************//
    queryTask = new esri.tasks.QueryTask(url + 29);

    //build query filter
    query = new esri.tasks.Query();
    query.returnGeometry = true;
    query.outFields = [
          "Name"
        , "SpaceNum"
        , "SpaceNumLetter"
        , "Address"
        , "BID"
        , "EGISID"
        ];

    //Accessibility Layers

    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer(url, {
        id: "entrances1",
        "imageParameters": imageParameters,
        visible: false
    });
      opLayer.setVisibleLayers([5,6]);
      dojo.connect(opLayer, "onError", reportError);
      map.addLayer(opLayer);


    opLayer = new esri.layers.FeatureLayer(url + 27, { id: "elevators1", visible: false });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);


    // Athletics Layers ///////////////////////////////////////////////////////////////////
    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer(url, {
        id: "athletics",
        opacity: .90,
        visible: false
    });
      opLayer.setVisibleLayers([36,37]);
      dojo.connect(opLayer, "onError", reportError);
      map.addLayer(opLayer);

    opLayer = new esri.layers.FeatureLayer(url + 47, {
        id: "jimClickHallFame",
        outFields: ["name"],
        visible: false
    });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);
    dojo.connect(opLayer, "onMouseOver", function (evt) {
        dialog.setContent(evt.graphic.attributes["name"]);
        dojo.style(dialog.domNode, "opacity", 0.85);
        dijit.popup.open({ popup: dialog, x: evt.pageX, y: evt.pageY });
    });
    dojo.connect(opLayer, "onMouseOut", function (evt) {dijit.popup.close(dialog);});
    map.addLayer(opLayer);
///////////////////////////////////////////////////////////////////////////////////////

    //Museums & Art Venues
    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer(url, {
        id: "museums",
        visible: false
    });
    opLayer.setVisibleLayers([30]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    //Health & Safety ///////////////////////////////////////////////////////////////////
    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer(url, {
        id: "healthSafetyBldgs",
        visible: false
    });
    opLayer.setVisibleLayers([31]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer(url, {
        id: "fitnessPaths",
        "imageParameters": imageParameters,
        visible: false
    });
    opLayer.setVisibleLayers([26,24]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer(url, {
        id: "safeRide",
        visible: false
    });
    opLayer.setVisibleLayers([43,44,45]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    opLayer = new esri.layers.FeatureLayer(url + 7, { id: "emergencyPhones1", visible: false });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);
    ///////////////////////////////////////////////////////////////////////////////////////

    //Libraries
    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer(url, {
        id: "libraries",
        visible: false
    });
    opLayer.setVisibleLayers([32]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);
    ///////////////////////////////////////////////////////////////////////////////////////

    //Parking
    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer(url, {
        id: "parkingLots",
        opacity: .75,
        visible: false
    });
    opLayer.setVisibleLayers([42]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer(url, {
        id: "parkingVisitors",
        "imageParameters": imageParameters,
        visible: false
    });
    opLayer.setVisibleLayers([40,41]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    opLayer = new esri.layers.FeatureLayer(url + 12, { id: "motorcycle1", visible: false });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    opLayer = new esri.layers.FeatureLayer(url + 13, { id: "bicycle1", visible: false });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    opLayer = new esri.layers.FeatureLayer(url + 11, { id: "disabledParking1", visible: false });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);
    ///////////////////////////////////////////////////////////////////////////////////////

    //Housing
    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer( url, {
        id: "housing_reslife",
        visible: false
    });
    opLayer.setVisibleLayers([38]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    opLayer = new esri.layers.FeatureLayer(url + 39, { id: "housing_greek", visible: false,
        mode: esri.layers.FeatureLayer.MODE_SNAPSHOT,
        outFields: ["Name", "Address", "GreekType"],
        infoTemplate: greekInfoTemplate
    });
    /*
    dojo.connect(opLayer, "onMouseOver", function (evt) {
        dialog.setContent(evt.graphic.attributes["Name"]);
        dojo.style(dialog.domNode, "opacity", 0.85);
        dijit.popup.open({ popup: dialog, x: evt.pageX, y: evt.pageY });
    });
    dojo.connect(opLayer, "onMouseOut", function (evt) {dijit.popup.close(dialog);});
    */

    opLayer.on("mouse-over",function(evt){
        //var highlightGraphic = new esri.Graphic(evt.graphic.geometry,highlightSymbol);
        var polygonSym = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, 
                                                        new esri.symbol.SimpleLineSymbol(
                                                            esri.symbol.SimpleLineSymbol.STYLE_SOLID, 
                                                            new dojo.Color([255,255,255,0.35]), 1), 
                                                        new dojo.Color([125,125,125,0.35]));
        //console.log("geometry:",evt.graphic.geometry);

        var highlightGraphic = new esri.Graphic(evt.graphic.geometry,polygonSym);
        map.graphics.add(highlightGraphic);
        dialog.setContent(evt.graphic.attributes["Name"]);
        dojo.style(dialog.domNode, "opacity", 0.85);
        dijit.popup.open({ popup: dialog, x: evt.pageX, y: evt.pageY });

        //console.log(map.graphics);
    });
    
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);
    ///////////////////////////////////////////////////////////////////////////////////////

    //Transportation
    opLayer = new esri.layers.FeatureLayer(url + 46, { id: "sunLinkStreetCarTracks", visible: false });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);


    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer( url, {
        id: "catTran",
        "imageParameters": imageParameters,
        visible: false
    });
    opLayer.setVisibleLayers([17,18,19,20,21,22,23]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    opLayer = new esri.layers.FeatureLayer(url + 16, { id: "bikeRoutes", visible: false });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);
    opLayer = new esri.layers.FeatureLayer(url + 10, { id: "sunLinkStreetCarStops1", visible: false });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);
    opLayer = new esri.layers.FeatureLayer(url + 9, { id: "sunTranStops1", visible: false });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);
    opLayer = new esri.layers.FeatureLayer(url + 8, { id: "shuttleStops", visible: false,
        mode: esri.layers.FeatureLayer.MODE_SNAPSHOT,
        outFields: ["*"],
        //infoTemplate: shuttleStopsInfoTemplate
    });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);
    ///////////////////////////////////////////////////////////////////////////////////////


    //Attractions 
    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer( url, {
        id: "historicBuildingsMuseums",
        visible: false
    });
    opLayer.setVisibleLayers([33]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer( url, {
        id: "museumsArtVenues",
        visible: false
    });
    opLayer.setVisibleLayers([30,62]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer( url, {
        id: "otherAttractionsMuseums",
        visible: false
    });
    opLayer.setVisibleLayers([61]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);
    ///////////////////////////////////////////////////////////////////////////////////////
    
    //Aerial360
    opLayer = new esri.layers.ArcGISDynamicMapServiceLayer(url, {
        id: "aerial360",
        visible: false
    });
    opLayer.setVisibleLayers([59]);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);
    
    //var markerSymbol = new esri.symbol.SimpleMarkerSymbol();
    //      markerSymbol.setPath('https://pdc-betagis2/public/img/360PhotosphereMainThick.svg');
          var markerSymbol = new esri.symbol.PictureMarkerSymbol('https://map.arizona.edu/img/360PhotosphereIcon.svg', 40, 40);
          polygonSymbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 0, 0]), 2), new dojo.Color([255, 0, 0, 0.55]));
       
          //markerSymbol.setColor(new Color(color));
          //markerSymbol.setOutline(null);
          //markerSymbol.setSize("32");

         // var smarkerSymbol = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE, 10, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 0, 0]), 1), new dojo.Color([0, 255, 0, 0.25]));

   
         var aerial360 = new esri.layers.FeatureLayer(url + 59, {
            id: "aerial361", mode: esri.layers.FeatureLayer.MODE_SNAPSHOT,  visible: false
          });
         map.addLayer(aerial360); 
       /*  */
    //var aerial360 = new esri.layers.FeatureLayer(url + 59, { id: "aerial360", mode: esri.layers.FeatureLayer.MODE_SNAPSHOT, surfaceType: "svg", visible: true });
    //dojo.connect(aerial360, "onError", reportError);
   // map.addLayer(aerial360);
    
    
    aerial360.on("load",function(){ 
        aerial360.renderer.symbol = markerSymbol;        
        var query = new esri.tasks.Query();
        query.where = "1=1";
        query.outFields = ['*'];
        aerial360.queryFeatures(query,function(fs){            
            fs.features.forEach(function(feature){
                feature.symbol = markerSymbol;
            });
           
        });          
        //aerial360.show();
    }); /**/
    /*
    var fl360 = new FeatureLayer("http://services.arcgis.com/V6ZHFr6zdgNZuVG0/arcgis/rest/services/fbTrim/FeatureServer/0", {
        id:"featureLayer"
    }
    */
    ///////////////////////////////////////////////////////////////////////////////////////

    //ON ALL THE TIME
/*  Construction
    opLayer = new esri.layers.FeatureLayer(url + 95, {
        id: "construction",
        mode: esri.layers.FeatureLayer.MODE_ONDEMAND,
        opacity: 0,
        outFields: ["ProjectName"]
    });
    dojo.connect(opLayer, "onMouseOver", function (evt) {
        dialog.setContent(evt.graphic.attributes["ProjectName"]);
        dojo.style(dialog.domNode, "opacity", 0.85);
        dijit.popup.open({ popup: dialog, x: evt.pageX, y: evt.pageY });
    });
    dojo.connect(opLayer, "onMouseOut", function (evt) {dijit.popup.close(dialog);});
    map.addLayer(opLayer);
*/

/*  Buildings
    opLayer = new esri.layers.FeatureLayer(url + 47, {
        id: "buildingsLayer",
        mode: esri.layers.FeatureLayer.MODE_SNAPSHOT,
        opacity: 0,
        maxAllowableOffset: 6,
        outFields: ["v_public_buildings.BL_NAME_PUBLIC", "Buildings.AliasName", "v_public_buildings.BL_ADDRESS_1", "Buildings.SpaceNum", "Buildings.SpaceNumLetter"],
        infoTemplate: bldgInfoTemplate
    });
    dojo.connect(opLayer, "onMouseOver", function (evt) {
        dialog.setContent(evt.graphic.attributes["v_public_buildings.BL_NAME_PUBLIC"]);
        dojo.style(dialog.domNode, "opacity", 0.85);
        dijit.popup.open({ popup: dialog, x: evt.pageX, y: evt.pageY });
    });
    //close maptip/tooltip on mouseout and click.
    dojo.connect(opLayer, "onMouseOut", function (evt) {dijit.popup.close(dialog);});
    dojo.connect(opLayer, "onClick", function (evt) {dijit.popup.close(dialog);});
    map.addLayer(opLayer);
*/
/*
    //Greek Houses
    opLayer = new esri.layers.FeatureLayer(url + 78, {
        id: "greekHousesMapTips",
        mode: esri.layers.FeatureLayer.MODE_ONDEMAND,
        opacity: 0,
        maxAllowableOffset: 6,
        outFields: ["Name", "Address", "GreekType"],
        infoTemplate: greekInfoTemplate
    });
    dojo.connect(opLayer, "onMouseOver", function (evt) {
        dialog.setContent(evt.graphic.attributes["Name"]);
        dojo.style(dialog.domNode, "opacity", 0.85);
        dijit.popup.open({ popup: dialog, x: evt.pageX, y: evt.pageY });
    });
    //close maptip/tooltip on mouseout and click.
    dojo.connect(opLayer, "onMouseOut", function (evt) {dijit.popup.close(dialog);});
    dojo.connect(opLayer, "onClick", function (evt) {dijit.popup.close(dialog);});
    map.addLayer(opLayer);

*/

    //Search Results
    searchResults = new esri.layers.GraphicsLayer({id: "searchResults"});
    map.addLayer(searchResults);
                
    roomPolygons = new esri.layers.GraphicsLayer({id: "roomPolygons"});
    map.addLayer(roomPolygons);

    roomLabels = new esri.layers.GraphicsLayer({id: "roomLabels"});
    roomLabels.minScale =  564.248588;
    map.addLayer(roomLabels);

    

    //////////////// AUDIBLE DEVICE CLASSROOMS /////////////////
    opLayer = new esri.layers.FeatureLayer(url + 2, { id: "classrooms1", visible: false
         , outFields: ["building_name", "ALSRoom_1"]
        // , infoTemplate: audibleInfoTemplate
    });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);
		dojo.connect(opLayer, "onMouseOver", function (evt) {
               dialog.setContent(evt.graphic.attributes["building_name"] + "<br>Rm: " + evt.graphic.attributes["ALSRoom_1"]);
               dojo.style(dialog.domNode, "opacity", 0.85);
               dijit.popup.open({ popup: dialog, x: evt.pageX, y: evt.pageY });
           });
           dojo.connect(opLayer, "onMouseOut", function (evt) {dijit.popup.close(dialog);});

    //////////////// RESTROOMS ALL GENDER /////////////////
    opLayer = new esri.layers.FeatureLayer(url + 55, { id: "allGens1", visible: false });    
    //console.log(url + 55);
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    //////////////// RESTROOMS WOMEN /////////////////
    opLayer = new esri.layers.FeatureLayer(url + 56, { id: "womenRes1", visible: false });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    //////////////// RESTROOMS MEN /////////////////
    opLayer = new esri.layers.FeatureLayer(url + 57, { id: "MenRes1", visible: false });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    //////////////// AED DEFIBULATORS /////////////////
    opLayer = new esri.layers.FeatureLayer(url + 3, { id: "aeds1", visible: false });
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    //Eateries
    opLayer = new esri.layers.FeatureLayer(url + 0, { id: "eateries1", visible: false, outFields:["Name"] });
    /*
    dojo.connect(opLayer, "onMouseOver", function (evt) {
        dialog.setContent(evt.graphic.attributes["Name"]);
        dojo.style(dialog.domNode, "opacity", 0.85);
        dijit.popup.open({ popup: dialog, x: evt.pageX, y: evt.pageY });
    });
    dojo.connect(opLayer, "onMouseOut", function (evt) {dijit.popup.close(dialog);});
    */
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer); 

    //////////////// OFF CAMPUS LANDMARKS //////////////////////
    opLayer = new esri.layers.FeatureLayer(url + 15, { id: "offCampusLandmarks", visible: false,
            mode: esri.layers.FeatureLayer.MODE_SNAPSHOT,
            outFields:["Name"]
    });
    dojo.connect(opLayer, "onMouseOver", function (evt) {
        // Get Coded Value Domains for Name field of 'OffCampusLandMarks'
        getCVD(url + 15, evt.graphic.attributes["Name"],function(res){     
                   
            dialog.setContent(res);            
            dojo.style(dialog.domNode, "opacity", 0.85);
            dijit.popup.open({ popup: dialog, x: evt.pageX, y: evt.pageY });
            
        });        
    });
    dojo.connect(opLayer, "onMouseOut", function (evt) {dijit.popup.close(dialog);});
    dojo.connect(opLayer, "onError", reportError);
    map.addLayer(opLayer);

    //toc_init();
    
   
}

dojo.addOnLoad(map_init);

function isLetter(str, roomNo) {
    var newRoom;
    if (!isNaN(parseInt(str, 10))) {
        //console.log(str+" is a Number")
        newRoom = '0'+roomNo;
        // Is a number
    } else {
        //console.log(str+" is a Letter")
        newRoom = roomNo;
    }
    return newRoom;
  }

function loadInterior(bldNo, roomNo, bldgAlph) {
    //console.log("roomNo:"+roomNo)
    clearGraphics();
    var totalAlphabets = '';
    roomNo = isLetter(roomNo.charAt(0), roomNo)
    //console.log("totalAlphabets::"+roomNo.replace(/[^A-Z]/gi, "").length);
    totalAlphabets = roomNo.replace(/[^A-Z]/gi, "").length;
    //console.log(roomNo +" - length:: "+roomNo.length +" -- char at 0:: "+roomNo.charAt(0))
    if(roomNo.length >= 5 && roomNo.charAt(0) === '0'){
        if (roomNo.match(/[A-Z]/i) && roomNo.length == 5 ) {
            var totalAlphabets = roomNo.replace(/[^A-Z]/gi, "").length;
            // alphabet letters found
        } else {
            roomNo = roomNo.substr(1);
        }
        //console.log("in the if roomNo::"+roomNo)
    }
    var firstLetter = roomNo.charAt(0)

    //console.log("1.First Character:"+firstLetter);                                
    var letters = /^[A-Za-z]+$/;

    //Append leadin 0 with string like 100E1 , in building 20 
    // !isNaN(roomNo.charAt(0) = Numeric value
    if(roomNo.charAt(roomNo.length-2).match(letters) && roomNo.charAt(0) !== '0' && !isNaN(roomNo.charAt(0)) == true) {
        //console.log("second last is letter");
        roomNo = '0'+roomNo;
    }

    // if last two characters are alphabets , and first charcter is not an alphabet, 
    // then append leading 0 with the room number
    if(totalAlphabets >=2 && firstLetter.match(letters) == null ){
        roomNo = '0'+roomNo;
    }

    // Add Dynamic Layers to Map    
    var featureLength = 0; 
    interiorLayers.forEach(function(lyrId){        

        var lyrURL = interiorServiceURL+lyrId;
        //console.log(lyrURL);
        var query = new esri.tasks.Query(); 
        //Add extra 0 with room Number in order to perform search
        //query.where = "ROOMEXT.bl_id ='"+blid+"' and ROOMEXT.RM_ID='" +roomNo+"'";
        query.where = "ROOMEXT.BldgAlpha ='"+bldgAlph+"' and ROOMEXT.RM_ID='" +roomNo+"'";
        //console.log(query.where);

        query.outFields = ["*"];
        query.returnGeometry = true;
        var queryTask = new esri.tasks.QueryTask(lyrURL);
        //console.log("Before:: "+featureLength);
        
        queryTask.execute(query, function (featureSet) {         
            featureLength = featureSet.features.length;         
            if (featureLength > 0) {
                mapFeature(featureSet.features, lyrId);
            }            
        });
        
    });
    saveLog = true;
    //submit rocord
    saveMetric(bldgAlph, roomNo);
}

function mapFeature(features, lyrId){
    //console.log("in the map features");
    features.forEach(function(feature){
        var attrib = feature.attributes;        
        
        //var polygonSymbol_ = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 0, 0]), 2), new dojo.Color([255, 0, 0]));
        feature.setSymbol(polygonSymbol);

        //console.log("Feature Name:: "+JSON.stringify(feature));
        //console.log(map.graphicsLayerIds);
        //console.log(map.layerIds );
        roomPolygons.add(feature);
        
        var ext = feature.geometry.getExtent();
        /*
        //console.log("Get Map Level Before:: "+map.getLevel());
        if(window.innerWidth < 500){ // for mobile view                                               
            map.setExtent(ext,true);                                          
        } */

        
        //console.log("Get Map Level Before:: "+map.getLevel());
        if(window.innerWidth < 500){ // for mobile view
            ext.ymin = ext.ymin+120;                                      
            map.setExtent(ext,true);                                          
        } else {
            map.setExtent(ext,true);  
        }
        
        // Label selected room 
        labelFeature(feature);

        var fl_id = attrib['ROOMEXT.fl_id'];
        var bl_id = attrib['ROOMEXT.bl_id'];
        if(fl_id){
           drawFloorPolygons(fl_id, bl_id, lyrId, feature)
        }
    });
}

function saveMetric(buildingAlpha, roomNumber){    
    if(saveLog == true)
    {
        //console.log("About enter record...")
        //console.log(buildingAlpha +" -- " +roomNumber);

        fetch("https://api.pdc.arizona.edu/user/publicmap/savemetric/",
            {
                headers: {      
                'Content-Type': 'application/json'
                //'database' : 'spacetest'      
                },
                method: "POST",    
                body: JSON.stringify({"buildingAlpha" : buildingAlpha, "roomNumber" : roomNumber})
            });
        saveLog = false;
    }
    

}

function drawFloorPolygons(floor, bldg, lyrId, feature){
    //searchResults.clear();
    map.infoWindow.hide();
    var lyrURL = interiorServiceURL+lyrId
    //console.log(lyrURL);
    var query = new esri.tasks.Query();
    //var queryUrl = lyrURL;
    query.where = "ROOMEXT.bl_id='"+bldg +"' and ROOMEXT.fl_id='" +floor+"'";
    //console.log(query.where);
    query.outFields = ["*"];
    query.returnGeometry = true;    
    var queryTask = new esri.tasks.QueryTask(lyrURL);
    //console.log("Before:: "+featureLength);    
    queryTask.execute(query, function (featureSet) { 
        //console.log(featureSet.features);
        var fts = featureSet.features
        fts.forEach(function(ft){
            //console.log("Attributes:: "+JSON.stringify(ft.attributes));
            var ext = ft.geometry.getExtent();  
            var gCenter = ext.getCenter();  
            if(ext.getWidth() > 3.8 && ft.attributes["ROOMEXT.RoomUse"] !== 'Circulation')
            {
                //console.log("RoomUse::"+ft.attributes["ROOMEXT.RoomUse"])
                var AcctText = new esri.symbol.TextSymbol();  
                AcctText.setFont(font);  
                AcctText.setColor(Textcolor);
                AcctText.setDecoration("none");
                var roomNum = ft.attributes['ROOMEXT.RM_ID'];
                if(roomNum.indexOf(0) == 0){                 
                    roomNum = roomNum.substr(1);                    
                }
                AcctText.setText(roomNum); 
                var gra = new esri.Graphic(gCenter,AcctText);
                roomLabels.add(gra);
            }

        });
        //roomPolygons.add(featureSet);
    })
    
    customFL = new esri.layers.FeatureLayer(lyrURL, {
        mode: esri.layers.FeatureLayer.MODE_ONDEMAND,
        id: "customFL",
        showLabels: true
    });

    customFL.setDefinitionExpression("ROOMEXT.bl_id='"+bldg +"' and ROOMEXT.fl_id='" +floor+"'");
    map.addLayer(customFL);
    map.reorderLayer(customFL,1);

    customFL.on("load",function(){
        //console.log("Custom FL Loaded");
        
        roomPolygons.add(feature);

        if(window.innerWidth < 500){ 
            $('.right-side-tools').toggleClass('visible', '1000');
        }
    });
    
}

function labelFeature(feature){
    
    var ext = feature.geometry.getExtent();  
    var gCenter = ext.getCenter();  
    var AcctText = new esri.symbol.TextSymbol();  
    AcctText.setFont(font);  
    AcctText.setColor(Textcolor);
    AcctText.setDecoration("none");
    var roomNum = feature.attributes['ROOMEXT.RM_ID'];
    if(roomNum.indexOf(0) == 0){                 
        roomNum = roomNum.substr(1);                    
    }

    AcctText.setText(roomNum);
    
    var gra = new esri.Graphic(gCenter,AcctText);
    roomLabels.add(gra);
}

function doSearch() {    
    console.log("do search....");
    if($("#searchbox").val() != '') {    
        //window.location.href = window.location.href + $("#searchbox").val();    
        
        doSearchById($("#searchbox").val());        
    }
    else {
        //"X" has been hit to clear searchbox
        console.log("In the doSearch ELSE"+","+$("#searchbox").val());
        searchResults.clear();

        map.infoWindow.hide();
    }
}

function getCVD(url, id, callback){
    var query = new esri.tasks.Query();
    var queryUrl = url;
    query.where = "1=1";
    query.outFields = ["*"];
    query.returnGeometry = false;    
    var queryTask = new esri.tasks.QueryTask(url);
    queryTask.execute(query, function (featureSet) {        
        fieldDomains = [];        
        esriRequestHandle = esri.request({
            url: queryUrl,
            content: { f: "json" },
            callbackParamName: "callback",
            load: function (res) {                
                dojo.forEach(res.fields, function (f) {
                    // Get code value domain for the name field only
                    if (f.name == "Name") {                        
                        fieldDomains[f.name] = f.domain.codedValues;
                        domainArray = f.domain.codedValues;                                      
                        domainArray.forEach(function(cvd){
                            if(cvd.code == id){
                                callback(cvd.name);                             
                            }
                        });                    
                   }
                });                
            },
            error: function (res) {
                console.log(err)
            }
        });         
    });
}
function clearGraphics(){
    //console.log(map.getLayer("customFL").id)
    if (map.getLayer("customFL") !== undefined){
        // Remove layer for next redraw
        map.removeLayer(map.getLayer("customFL"))        
    }
    try {
        searchResults.clear();
        roomLabels.clear();
        roomPolygons.clear();
    }
    catch(err) {
      console.log(err.message);
    }
}

function doSearchById(id) {
    //dojo.require("esri.tasks.find");
    //dojo.require("esri.tasks.FindTask");
        
       // window.location.href = window.location.origin+'/'+$("#searchbox").val(); //prod server url
    //url = restEndpoint+"publicDynamic/MapServer/";
    
        try {
            clearGraphics();
        } catch(err) {
            console.log(err.message);
        }
        var findTask = new esri.tasks.FindTask(url);        
        //console.log("find Task :: "+url +", id: "+id);
        //create find parameters and define known values
        var findParams = new esri.tasks.FindParameters();
        findParams.contains = false;
        findParams.returnGeometry = true;
        findParams.layerIds = [29];
        findParams.searchFields = ["SpaceNumLetter"];
        //findParams.searchFields = ["EGISID"];
        //set the search text to find parameters
        
        findParams.searchText = id;
        //console.log(findParams);        
        findTask.execute(findParams, showResultsFindTask);
}

function executeQueryTask(evt) {    
    selectedGraphics = '';
    
    var isAerilClick = 0;
    query.geometry = evt.mapPoint;
    //queryTask.execute(query, showResultsQueryTask);

    map.infoWindow.hide();
    //All for New Identify Task
    identifyParams.geometry = evt.mapPoint;
    identifyParams.mapExtent = map.extent;
    var layerName = "",
        response = "",
        bldgInfoTemplate = "";
    resbuildingFlag = 2;    
    //console.log("resbuildingFlag::: "+resbuildingFlag)
    deferred = identifyTask
            .execute(identifyParams, function (idResults) {
                addToMap(idResults, event);
              })
            .addCallback(function (response) {
                
                // response is an array of identify result objects
                // Let's return an array of features.               
              
                return dojo.map(response, function (result) {
                    var feature = result.feature;
                    layerName = result.layerName;

                    //map.infoWindow.resize(210, 270);
                    map.infoWindow.resize(230, 290);                    
                    feature.attributes.layerName = layerName;
                    if (layerName === 'All Gender Restrooms' || layerName === "Women's Restrooms" || layerName === "Men's Restrooms" ){
                        bldgInfoContent = '';    
                        //console.log(JSON.stringify(feature.attributes)+" - "+feature.attributes['BuildingName'])                        
                        
                        if(feature.attributes['BuildingName'] != "Null") {
                            bldgInfoContent = "<div style='line-height: normal; margin:0px 0px 4px 0' >${BuildingName} ";
                            if (feature.attributes["RoomID"] !== "Null") {
                                bldgInfoContent += " &nbsp${RoomID}";
                            }
                            bldgInfoContent += "</div>";
                        }

                        if (feature.attributes["FloorName"] !== "Null") {
                            bldgInfoContent += "<div class='bldg-num'>Floor ${FloorName} </div>";
                        }

                        if (feature.attributes["IsPublic"] !== "Null") {
                            if(feature.attributes["IsPublic"] == "No") {
                                bldgInfoContent += "<div class='bldg-num'>Not publicly accessible </div>";
                            } else if (feature.attributes["IsPublic"] == "Yes") {
                                bldgInfoContent += "<div class='bldg-num'>Publicly accessible </div>";
                            }                            
                        }

                        if (feature.attributes["Restrictions"] !== "Null") {
                            bldgInfoContent += "<div class='bldg-num'>${Restrictions} </div>";
                        }

                        if (feature.attributes["Location Notes"] !== "Null") {
                            //bldgInfoContent += "<div class='bldg-num' style='line-height: normal;'>Location Notes:${Location Notes} </div>";
                            bldgInfoContent += "<div class='bldg-num' style='line-height: normal; margin:4px 0px 0 0'>Location Notes:${Location Notes}</div>";
                        }
                        bldgInfoTemplate = new esri.InfoTemplate("<b>${RestroomType} Restroom</b>", bldgIunshiftnfoContent);
                        feature.setInfoTemplate(bldgInfoTemplate);
                    }

                    if (layerName.includes('Museums and Art Venues') || layerName === 'Other Visitor Center Attractions'){                            
                        bldgInfoContent = "<img src='https://maps.arizona.edu/photos/buildings/small/${Building Number}.jpg' onerror='missingBuildingPhoto(this);' height='138' width='207'>";
                                        //+ "<div class='bldg-num'> AKA: ${AliasName} </div>"                                        
                                        if (feature.attributes["Tour Category"] !== "Null") {
                                            bldgInfoContent += "<div class='bldg-num'> <b> ${Tour Category} </b> </div>"
                                        }
                                        if (feature.attributes["Closest Parking"] !== "Null") {
                                            bldgInfoContent += "<div class='bldg-num'> <b> Closest Parking:</b>  ${Closest Parking} </div> "
                                        }
                                        if (feature.attributes["Description"] !== "Null") {
                                            bldgInfoContent += "<div class='bldg-num' style='padding-top: 5px;'> ${Description} </div>"
                                        }
                                        if (feature.attributes["Address"] !== "Null") {
                                            bldgInfoContent += "<div class='bldg-num'> <b>Address:</b> ${Address} </div>"
                                        }
                                        if (feature.attributes["Hours"] !== "Null" && feature.attributes["Hours"].length > 2) {
                                            bldgInfoContent += "<div class='bldg-num'> <b>Hours:</b> ${Hours} </div>"
                                        }
                                        if (feature.attributes["Phone Number"] !== "Null") {
                                            bldgInfoContent += "<div class='bldg-num'> <b>Phone:</b> ${Phone Number} </div>"
                                        }

                        var Name = feature.attributes["Name"];
                        bldgInfoTemplate = new esri.InfoTemplate("<b>"+Name+"</b>", bldgInfoContent);
                        feature.setInfoTemplate(bldgInfoTemplate);
                    } else if (layerName === 'UA Residence Life Housing'){                            
                            bldgInfoContent = "<img src='https://maps.arizona.edu/photos/buildings/small/${SpaceNumLetter}.jpg' onerror='missingBuildingPhoto(this);' height='138' width='207'>"
                                            //+ "<div class='bldg-num'> AKA: ${AliasName} </div>"
                                            + "<div class='bldg-num'> <b>Address:</b> ${Address} </div>"
                                            + "<div class='bldg-num'> Building Number: ${SpaceNumLetter} </div>";

                            var dormName = feature.attributes["Name"];
                            dormName = dormName.replace('Residence Hall','');
                                                            
                            bldgInfoTemplate = new esri.InfoTemplate("<a href='${BuildingUserURL}' target='_blank'><b>"+dormName+"</b></a>", bldgInfoContent);
                            feature.setInfoTemplate(bldgInfoTemplate);
                        }
                        
                        else if (layerName === 'BuildingsResLifeMerged' && map.getLayer("housing_reslife").visible == true) {                            
                            resbuildingFlag =  1;    
                            var dormName1 = feature.attributes["Name1"];
                            dormName1 = dormName1.replace('Residence Hall','');
                            var dormName2 = feature.attributes["Name2"];
                            dormName2 = dormName2.replace('Residence Hall','');
                            bldgInfoContent = "<img src='https://maps.arizona.edu/photos/buildings/small/${SpaceNumLetter}.jpg' onerror='missingBuildingPhoto(this);' height='138' width='207'>"
                                            + "<div class='bldg-num'> <b>Address:</b> ${Address} </div></br>"
                                            + "<div class='bldg-num'> <b>"+dormName1+"</b> </div>"
                                            + "<div class='bldg-num'> &nbsp;&nbsp;&nbsp;Building Number: ${SpaceNumLetter} </div>"
                                            + "<div class='bldg-num'> <b>"+dormName2+"</b> </div>"
                                            + "<div class='bldg-num'> &nbsp;&nbsp;&nbsp;Building Number: ${SpaceNumLetter2} </div>";

                            var dormName = feature.attributes["Name"];
                            dormName = dormName.replace('Residence Hall','');

                            bldgInfoTemplate = new esri.InfoTemplate("<a href='${URL}' target='_blank'><b>"+dormName+"</b></a>", bldgInfoContent);
                            feature.setInfoTemplate(bldgInfoTemplate);                            
                        } else if (layerName === 'Buildings') {                             
                            //bldgInfoContent = "<img src='https://maps.arizona.edu/photos/buildings/small/${SpaceNum}.jpg' onerror='missingBuildingPhoto(this);' height='120' width='179'>"
                            bldgInfoContent = "<img src='https://maps.arizona.edu/photos/buildings/small/${SpaceNumLetter}.jpg' onerror='missingBuildingPhoto(this);' height='138' width='207'>";
                                            if (feature.attributes["AliasName"] !== "Null"){
                                                bldgInfoContent += "<div class='bldg-num'> AKA: ${AliasName} </div>";
                                            }
                                            bldgInfoContent += "<div class='bldg-num'> Address: ${Address} </div>";
                                            bldgInfoContent += "<div class='bldg-num'> Building Number: ${SpaceNumLetter} </div>";
                            
                            var Name = feature.attributes["Name"];
                            Name = Name.replace('Residence Hall','');

                            if (feature.attributes["ShowMe"] == "Campus Housing" || feature.attributes["ShowMe2"] == "Campus Housing"){
                                bldgInfoTemplate = new esri.InfoTemplate("<a href='${BuildingUserURL}' target='_blank'><b>"+Name+"</b></a>", bldgInfoContent);
                            } else {
                                //If name is null print not name in the info window                                
                                if(Name == 'Null') {
                                    Name = '';
                                }
                                bldgInfoTemplate = new esri.InfoTemplate("<b>"+Name+"</b>", bldgInfoContent);
                            }
                            feature.setInfoTemplate(bldgInfoTemplate);
                            resbuildingFlag = 0;
                        } else if (layerName === 'Places To Eat1128') {   
                            //console.log("layerName:: "+layerName)
                            var attributes = feature.attributes;
                            //console.log("Attributes :: ",JSON.stringify(attributes));
                            var floorName, Name,bldgInfoContent;
                            //bldgInfoContent = "<img src='https://maps.arizona.edu/photos/buildings/small/${SpaceNum}.jpg' onerror='missingBuildingPhoto(this);' height='120' width='179'>"
                            
                            if (attributes["FloorName"] !== "Null") {
                                floorName = ", Floor "+attributes["FloorName"];
                                //floorName +=  floorName
                            }                            
                            if (attributes["BuildingName"] !== "Null") {
                                bldgInfoContent = "<div class='bldg-num'>${BuildingName}"+floorName +" </div>";
                            }
                            if (attributes["Address"] !== "Null") {
                                bldgInfoContent += "<div class='bldg-num'> <i> ${Address} </i></div>";
                            }
                            bldgInfoContent += "<br/><img  height='70px' width='207px' src='https://maps.arizona.edu/photos/PointsOfInterest/${Photo}' onerror='missingBuildingPhoto(this);'/>";                                            
                            
                            if (attributes["PhoneNumber"] !== "Null") {
                            
                                bldgInfoContent += "<div class='bldg-num'><b>${PhoneNumber}</b></div>";
                            }

                            bldgInfoContent += "<div class='bldg-num'><a href='https://union.arizona.edu/infodesk/hours/index.php?cat=dining' target='_blank'>Today's Hours</a></div>";

                            if (attributes["url"] !== "Null") {
                                bldgInfoContent += "<div class='bldg-num'><a href='${url}' target='_blank'>Website</a> </div>";
                            }

                            //bldgInfoTemplate = new esri.InfoTemplate("<b>"+Name+"</b>", bldgInfoContent);
                            bldgInfoTemplate = new esri.InfoTemplate("<b>"+attributes["name"]+"</b>", bldgInfoContent);
                            feature.setInfoTemplate(bldgInfoTemplate);
                            
                        }
                        else if (layerName === 'Shuttle Stops1128') {
                            var attributes = feature.attributes;
                            var floorName, Name,bldgInfoContent;
                            bldgInfoContent = '';
                            if (attributes["TimedStopName"] !== "Null" && attributes["TimedStopName"] !== undefined) {
                                bldgInfoContent += "<div class='bldg-num'>Timed Stop:  ${TimedStopName} </div>";
                            }
                            
                            if (attributes["RoutesServed"] !== "Null") {
                            
                                bldgInfoContent += "<div class='bldg-num'>Routes Served: ${RoutesServed}</div>";
                            }
                            bldgInfoContent += "Schedule Info: <a href='https://parking.arizona.edu/campus-services/cattran/' target='_blank'>Route Schedules</a> </p>";
                            //bldgInfoTemplate = new esri.InfoTemplate("<b>"+Name+"</b>", bldgInfoContent);
                            bldgInfoTemplate = new esri.InfoTemplate("<b>Cat Tran Stop Info</b>", bldgInfoContent);
                            feature.setInfoTemplate(bldgInfoTemplate);                            
                        } else if (layerName === 'Aerial360Points') {
                            isAerilClick = 1;
                            //var content = "";
                            console.log("in the aerial 360");
                            /**************** */
                            if (attributes["Scene"] !== "Null") {
                                
                                if (window.screen.availWidth >= 2000)    {
                                    w = 1700
                                    h = 1000
                                } else {
                                    w = 1200
                                    h = 650
                                }
                                

                                
                                var url = attributes["Scene"];

                            var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
                            var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;
                        
                            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
                        
                            var left = ((width / 2) - (w / 2)) + dualScreenLeft+20;
                            var top = ((height / 2) - (h / 2)) + dualScreenTop+100;

                            console.log("height / 2="+height+"top::"+top);
                            window.open(url, "title", 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

                            //var newWindow = window.open(url, "title", 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
                            
                            // Foucusing out main app creates an error
                            /*
                            // Puts focus on the newWindow
                            if (window.focus) {
                                if(newWindow) {
                                    newWindow.focus();
                                } else {
                                    $('<div class="has-error"><p class="help-block">Pop Up blocked. Please disable your pop up blocker to provide your bank details.</p></div>').insertBefore('.refNo');
                                }
                            }
                            */
                            //return newWindow;


                            }
                        }
                   
                    if (layerName === 'ConstructionFences') {
                        constBoundaryInfoContent = "";
                        var ProjectNumber = feature.attributes.PDCProjectNumber;
                        var ProjectName = feature.attributes.ProjectName;
                        if (ProjectNumber !== null || ProjectNumber !== '') {
                            var projNum = "${PDCProjectNumber}";
                            constBoundaryInfoContent += "<div class='bldg-num'> PDC Project #: ${PDCProjectNumber} </div>";
                        }
                        if ('${date_added}' !== null || '${date_added}' !== '') {
                            constBoundaryInfoContent += "<div class='bldg-num'> Date Added: ${date_added} </div>";
                        }
                        
                        if (ProjectNumber.length > 4) {                          
                            projectsDialog = new dijit.Dialog({
                                title: ProjectName+ " - " +ProjectNumber,
                                content: "<iframe id='ifra' frameborder='0' src='https://www.pdc.arizona.edu/projectmapflyout/"+ProjectNumber+"'  width='800px' height='1000px' style:'overflow:hidden' ></iframe>",                                                              
                            });

                            constBoundaryInfoContent += "<a style='color:#7F5959' onclick=projectsDialog.show(); class='projectDetails' href='#'>More Info</a></p>";
                        }
                        
                        var constInfoTemplate = new esri.InfoTemplate("<b>${ProjectName}</b>", constBoundaryInfoContent);
                        feature.setInfoTemplate(constInfoTemplate);
                    }
                    if (feature) {
                        if(isAerilClick == 0){
                            map.infoWindow.show(evt.mapPoint);
                        } 
                        /*
                        else {
                           // isAerilClick = 0;
                        }*/
                            
                        
                    } 


                    if(feature.infoTemplate != undefined)
                        {
                            return feature;
                        }
                    else {
                            return false;
                        }    
                    

                });
            })
            if(isAerilClick == 1){
                isAerilClick = 0;
            }

            var resultsArray = [];
            
            /*
            identifyTask.on("complete", function(results){
                console.log("id task..");
                //searchResults.clear();  
                  //map.removeLayer(searchResults);
                  
                    map.infoWindow.setFeatures([deferred]);
                    console.log(results.results[0].feature.attributes.EGISID);
                    if(results.results[0].feature.attributes.EGISID){
                        selectedGraphics = results.results[0].feature.attributes.EGISID;
                    }

                //searchResults.clear();
                //console.log("results ::"+JSON.stringify(results.results[0]));
                    //map.addLayer(searchResults); 
                //searchResults.add(results);  //important for share tool
                //console.log(searchResults.graphics);
            });
            */
            //map.infoWindow.show(evt.mapPoint);         
            //searchResults.add(feature);  
            
                
    /**************************************************************************/
    //queryTask.execute(query, showResultsQueryTask);
}

function addToMap (idResults, event) {
    map.infoWindow.setFeatures([deferred]);
    
    searchResults.add(idResults);
    console.log(idResults);
    /*
    if(idResults[0].layerId == 29){
        selectedGraphics = idResults[0].feature.attributes.BID;
    } else     
    */
    if(idResults[0].feature.attributes.EGISID){
        selectedGraphics = idResults[0].feature.attributes.EGISID;
        //console.log('selectedGraphics:: ',selectedGraphics);   /* */
    }
    
}
 function showResultsQueryTask(featureSet) {
    //console.log(JSON.stringify(featureSet));
     for (var i=0, il=featureSet.features.length; i<il; i++) {
         console.log(i+". "+JSON.stringify(featureSet.features[i]));
     }
    /*
    var polygonSymbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 0, 0]), 2), new dojo.Color([255, 0, 0, 0.55]));
    searchResults.clear();

    var bldgInfoTemplate = new esri.InfoTemplate("<b>${Name}</b>", bldgInfoContent);
    
    //QueryTask returns a featureSet.  Loop through features in the featureSet and add them to the map.
    for (var i=0, il=featureSet.features.length; i<il; i++) {
        //Get the current feature from the featureSet.
        //Feature is a graphic
        var graphic = featureSet.features[0];
        graphic.setSymbol(polygonSymbol);

        map.setExtent(graphic.geometry.getExtent(),true);

        //show info window for only polygons... may change later.
        if (map.infoWindow.isShowing) {
            map.infoWindow.hide();
        }

        //Assuming all queries are buildings... set infoTemplate
        graphic.setInfoTemplate(bldgInfoTemplate);

        map.infoWindow.setContent(graphic.getContent());
        map.infoWindow.setTitle(graphic.getTitle());
        map.infoWindow.show(graphic.geometry.getExtent().getCenter());
    }
    searchResults.add(graphic);
    */
}

function showResultsFindTask(results) {    
    //console.log("showResultsFindTask");
    //console.log(showResultsFindTask);
    map.graphics.clear();    
    //symbology for graphics
    var markerSymbol = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE, 10, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 0, 0]), 1), new dojo.Color([0, 255, 0, 0.25]));
    var lineSymbol = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_DASH, new dojo.Color([255, 0, 0]), 1);
    //polygonSymbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 0, 0]), 2), new dojo.Color([255, 0, 0, 0.55]));
    
    var selectedValue = $("#searchbox").val();

    //find results return an array of findResult.
    //Following line was creating a bug by not showing info window when user closes buildings info -> Search 

    //Build an array of attribute information and add each found graphic to the map    
    dojo.forEach(results, function(result) {
        if(result.feature){
            setTimeout(function() {                
                if(result.feature.attributes.SpaceNumLetter && saveLog == true){
                    saveMetric(result.feature.attributes.SpaceNumLetter, '')
                }
                saveLog = true;
                
            }, 12000);
            
        }
        var graphic = result.feature;        
        searchResults.clear();
        //TODO: check the BID for a match in the querystring.
        //TODO: need a better conditional than than the querystring check... will not zoom to a search, if Id is provided.
        
        switch (graphic.geometry.type) {
            
            case "point":                
                graphic.setSymbol(markerSymbol);
                if(qsHasID == false) //the extent is in the querystring, no need to zoom
                    map.centerAndZoom(graphic.geometry.GetPoint(),18);
                break;
            case "polyline":
                graphic.setSymbol(lineSymbol);
                if(qsHasID == false) //the extent is in the querystring, no need to zoom
                    map.setExtent(graphic.geometry.getExtent(),true);
                break;
            case "polygon":                  
                graphic.setSymbol(polygonSymbol);
                //console.log("ID param?" + qsHasID);
                //if(qsHasID == false) //the extent is in the querystring, no need to zoom
                //show info window for only polygons... may change later.
                //console.log("infow window showing:: "+map.infoWindow.isShowing);
                if (map.infoWindow.isShowing) {
                    //Following line was creating a bug by not showing info window when user closes buildings info -> Search -> Search
                    //map.infoWindow.hide();
                }

                attributes = graphic.attributes;
                attributes['Name'] = attributes['Name'].replace('Residence Hall','');

                var bldgInfoTemplate = new esri.InfoTemplate("<b>"+attributes['Name']+"</b>", bldgInfoContent);
                bldgInfoContent = "<img src='https://maps.arizona.edu/photos/buildings/small/"+attributes['SpaceNumLetter']+".jpg' onerror='missingBuildingPhoto(this);' height='138' width='207'>";
                //console.log("Alias Name::"+attributes['AliasName']);
                if(attributes['AliasName'] !== "Null"){
                    bldgInfoContent += "<div class='bldg-num'> AKA: "+attributes['AliasName']+" </div>";
                }
                bldgInfoContent +=  "<div class='bldg-num'> Address: "+attributes['Address']+" </div>";
                bldgInfoContent +=  "<div class='bldg-num'> Building Number: "+attributes['SpaceNumLetter']+" </div>";//SpaceNumLetter

                //Assuming all searches are buildings... set infoTemplate                
                graphic.setInfoTemplate(bldgInfoTemplate);
                //map.infoWindow.resize(200, 270);
                map.infoWindow.resize(230, 290);
                map.infoWindow.setContent(bldgInfoContent);              
                //map.infoWindow.setContent(graphic.getContent());
                map.infoWindow.setTitle(graphic.getTitle());  
                map.infoWindow.show(graphic.geometry.getExtent().getCenter());
                var ext = graphic.geometry.getExtent();

                
                //console.log("Get Map Level Before:: "+map.getLevel());
                if(window.innerWidth < 500){ // for mobile view
                    ext.ymin = ext.ymin+120;                                      
                    map.setExtent(ext,true);                                          
                } else {
                    map.setExtent(ext,true);  
                }
                searchResults.add(graphic);                                
                break;
        }
        
        
        if (map.infoWindow.isShowing === true) {                
                $('#searchbox').val(selectedValue);
                $('#searchbox').trigger("liszt:updated"); // Maintains value in the search box 
        }
       
        qsHasID = false;
    });
}

function createPrintDijit(printTitle) {
    var printer,layoutTemplate, templateNames, mapOnlyIndex, templates;

    //esri.config.defaults.io.proxyUrl = "/arcgisserver/apis/javascript/proxy/proxy.ashx";

    // create an array of objects that will be used to create print templates
    var layouts = [
      {
        "name": "Letter ANSI A Landscape",
        "label": "Landscape (image)",
        "format": "png32",
        "options":  {
          "legendLayers": [],
          "scaleBarUnit": "Feet",
          "titleText": printTitle
        }
      },
      {
        "name": "Letter ANSI A Portrait",
        "label": "Portrait (image)",
        "format": "png32",
        "options":  {
          "legendLayers": [],
          "scaleBarUnit": "Feet",
          "titleText": printTitle
        }
      },
      {
        "name": "Letter ANSI A Landscape",
        "label": "Landscape (PDF)",
        "format": "pdf",
        "options": {
          "legendLayers": [], // empty array means no legend
          "scalebarUnit": "Feet",
          "titleText": printTitle
        }
      },
      {
        "name": "Letter ANSI A Portrait",
        "label": "Portrait (PDF)",
        "format": "pdf",
        "options": {
          "legendLayers": [], // empty array means no legend
          "scalebarUnit": "Feet",
          "titleText": printTitle
        }
      }
    ];

    // create the print templates, could also use dojo.map
    var templates = [];
    dojo.forEach(layouts, function(lo) {
      var t = new esri.tasks.PrintTemplate();
      t.layout = lo.name;
      t.label = lo.label;
      t.format = lo.format;
      t.layoutOptions = lo.options
      templates.push(t);
    });

    printer = new esri.dijit.Print({
      map: map,
      templates: templates,
      url: "https://services.maps.arizona.edu/pdc/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task"}, dojo.byId("candyBar"));
      printer.startup();
}

//todo: not implemented yet!
function mapTipByName(oplayer,dialog) {
        //"open" maptip/tooltip on mouseover
    dojo.connect(opLayer, "onMouseOver", function (evt) {
        dialog.setContent(evt.graphic.attributes["Name"]);
        dojo.style(dialog.domNode, "opacity", 0.85);
        dijit.popup.open({ popup: dialog, x: evt.pageX, y: evt.pageY });
    });

    //close maptip/tooltip on mouseout and click.
    dojo.connect(opLayer, "onMouseOut", function (evt) {dijit.popup.close(dialog);});
    dojo.connect(opLayer, "onClick", function (evt) {dijit.popup.close(dialog);});
}

function missingBuildingPhoto(image) {
    image.onerror = "";
    image.src = "img/noimage.gif";
    return true;
}

function getLiveGPSFeed(routeIds){
    if(getGPSFeed === true) {
        ajaxGPSRequest = $.ajax({
            type: "GET",
            dataType:'json',
            data: {},
            url: 'https://transloc-api-1-2.p.mashape.com/vehicles.json?agencies=371&callback=successGPSRequest&routes='+routeIds,
            success: successGPSRequest,
            error: errorGPSRequest,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-Mashape-Authorization', 'LkxL7NhCRxEnnkB5frdi3i4iprLYgS6h');
            }
        });
    }
}

function errorGPSRequest(XMLHttpRequest, textStatus, errorThrown, routeIds){
    setTimeout('getLiveGPSFeed([' + routeIds + '])',1000);
    console.log(textStatus+" "+errorThrown);
}

function successGPSRequest(data){
    // do what you need with the returned data...
    setTimeout('getLiveGPSFeed([' + routeIds + '])',1000);
    map.graphics.clear();

    for (var i=0;i<data.data[371].length;i++){
        lat=data.data[371][i].location.lat;
        lng=data.data[371][i].location.lng;
        var liveBusSymbol;

        switch(data.data[371][i].route_id){
            case "8004294": //USA/Downtown
                liveBusSymbol=new esri.symbol.PictureMarkerSymbol('img/bus-blue.png', 40, 65);
                break;
            case "8004210": //Outer Campus Loop
                liveBusSymbol=new esri.symbol.PictureMarkerSymbol('img/bus-green.png', 40, 65);
                break;
            case "8004212": //North/South
                liveBusSymbol=new esri.symbol.PictureMarkerSymbol('img/bus-purple.png', 40, 65);
                break;
            case "8004218": //Mountain Avenue
                liveBusSymbol=new esri.symbol.PictureMarkerSymbol('img/bus-orange.png', 40, 65);
                break;
            case "8004214": //Inner Campus Loop
                liveBusSymbol=new esri.symbol.PictureMarkerSymbol('img/bus-yellow.png', 40, 65);
                break;
            case "8002016": //Nightcat
                liveBusSymbol=new esri.symbol.PictureMarkerSymbol('img/bus-darkgreen.png', 40, 65);
                break;
            case "8004220": //SE Off Campus/UA Mall
                liveBusSymbol=new esri.symbol.PictureMarkerSymbol('img/bus-red.png', 40, 65);
                break;
        }
        liveBusSymbol.yoffset = 33;
        mapPoint=new esri.geometry.Point(lng,lat);
        map.graphics.add(new esri.Graphic(mapPoint,liveBusSymbol));
    }
}

function stopLiveGPSFeed(){
    if (ajaxGPSRequest) {
        getGPSFeed = false;
        ajaxGPSRequest.abort();
        map.graphics.clear();
    }
}

function reportError(error) {
  $("#errorMessage").val(error.message);
  var formData = new FormData($('#customError')[0]); // http://stackoverflow.com/questions/11341067/ajax-form-submit-with-file-upload-using-jquery
  // AJAX request to email the error message using the Python script: mail_error.py
  $.ajax({
    url:'py/mail_error.py',
    type: 'POST',
    data: formData,
    async: false,

    success: function (data) {
      //redirect users to the UA-themed error page
      window.location="https://www.arizona.edu/map-temporarily-out-service";
    },

    fail:function(jqXHR, textStatus) {
      //if for some reason the email didn't send, we can do things here, otherwise redirect to the UA-themed error page
      window.location="https://www.arizona.edu/map-temporarily-out-service";
    },
    cache: false,
    contentType: false,
    processData: false
  });
}