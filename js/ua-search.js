
    var selectedBuildingAlpha;
    var selectedBuilding;
    var buildingsJSON;
    var selectedValue                                                                                                                               
    var buildings = [];
    var buildinglist = [];
    var buildingJSON;
    var chooseTrigger = true;    
    var oldRoomVal = '';

    function loadRooms(building) {
       
            var match = searchArray(building, buildinglist);
            console.log("match is::" + building+" -- "+buildinglist.length);
            //console.log(match.bldgUse);

            if(match.bldgUse !== 4)
            {   console.log("in the if ....")
                 $('#roominput').prop('disabled', false);
                 $('#searchMsg').css('display', 'none');
            } else {
                $('#roominput').prop('disabled', true);
                $('#searchMsg').css('display', 'block');
            }
            
            selectedValue = building;
            // Get Building Alpha value from buildingsJSON
            $.each(buildings, function () {
                //console.log(this);
                if (this['bldgNum'] == building) {
                    selectedBuildingAlpha = this['bldgAlpha'];
                    //console.log("Building Alpha :: "+this['bldgAlpha']);
                };
            });

            console.log(window.location);
            //Trigger Building Search
            //window.location.href = window.location.origin+'/'+window.location.pathname.split('/')[1]+'/'+selectedBuildingAlpha; //beta server url
            window.history.pushState("object or string", "Page Title", window.location.origin+'/'+window.location.pathname.split('/')[1]+'/'+selectedBuildingAlpha);
            console.log("selectedBuildingAlpha:",selectedBuildingAlpha);
            doSearchById(selectedBuildingAlpha);       
            
            $("#roominput").val('');

            /*
            var foption = $('#dropDownRooms option:first');
            $("#dropDownRooms").empty();*/

            var roomsList = [];
            

            $.getJSON("https://api.pdc.arizona.edu/rooms/" + selectedValue + "/", function (obj) {                    
                $.each(obj, function (key, value) {
                    //var option = $('<option />').val(value.number).text(value.number);
                    var roomNum = value.number
                    if(roomNum.indexOf(0) == 0){
                        //if first value is 0
                        roomNum = roomNum.substr(1);                    
                    }
                    //console.log(roomsList);
                    roomsList.push(roomNum);
                });
            });

            //$('#dropDownRooms').prepend(foption); 
            //$('#dropDownRooms option:first').prop('selected', 'selected');

            var roomoptions = {
                data: roomsList,
                adjustWidth: false,
                list: {  maxNumberOfElements: 100, 
                            match: { 
                                enabled: true,
                                method:  function(element, phrase) {
                                    var spacephrase = new Array(1 + 1).join(' ')+phrase ;
                                    //console.log("1.phrase:"+phrase);
                                    var firstLetter = phrase.charAt(0);  
                                    var elementFirstLetter = element.charAt(0)                          

                                    var letters = /^[A-Za-z]+$/;
                                    if(element.indexOf(phrase) === 0) {
                                        //return only first match letter results
                                        return true;
                                    } 
                                    // Search for first letter, if its an alphabet ignore and start search preceeding letters
                                    else if (firstLetter.match(letters) == null && element.indexOf(phrase) === 1 && elementFirstLetter.match(letters) !== null ){
                                        //console.log("element ::"+element);
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                } 
                                
                            },
                            onChooseEvent: function() {   
                                if(chooseTrigger == true){
                                    console.log("pick room...!")
                                    var room = $("#roominput").val();          
                                    console.log(window.location);
                                    //console.log("selectedValue:"+selectedValue+ "- room:"+room+ "- selectedBuildingAlpha:"+selectedBuildingAlpha)
                                    loadInterior(selectedValue, room, selectedBuildingAlpha);
                                    let newURL = window.location.origin+'/'+window.location.pathname.split('/')[1]+'/'+selectedBuildingAlpha+'-'+room; //beta server
                                    window.history.pushState("object or string", "Page Title", newURL);
                                    //window.location.href = window.location.origin++selectedBuildingAlpha+'-'+room; //prod server
                                }  // Load Interior Data
                                chooseTrigger = true;
                            }                                         
                        }
            }; 
            
    
            if(match.bldgUse !== 4)
            {   
                // $('#roominput').prop('disabled', false);
                $("#roominput").easyAutocomplete(roomoptions);
                $("#ResultsSection").show();
            }
    };

    $(function(){        

        $("#roominput").keydown( function(key){
            
            if(key.keyCode == 13) {
                //console.log("room input key down...!"+key.keyCode);
                var room = $("#roominput").val();
                if(room !== oldRoomVal) // Load room  data upon uniqe value
                {
                    loadInterior(selectedValue, room)
                    chooseTrigger = false;
                }
                oldRoomVal = room;
                
                setTimeout(function(){
                    chooseTrigger = true;
                }, 500)
            }
        })

    });    

    function sortDropDownListByText(selected) {
        //console.log("Sort: " + selected);
        selected.html($("option", selected).sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }));
    };


   //$.getJSON("https://pdc-betagis2.catnet.arizona.edu:6443/arcgis/rest/services/publicDynamic/MapServer/29/query?where=1%3D1&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&f=json", function (obj) {    
    $.getJSON("https://services.maps.arizona.edu/pdc/rest/services/publicDynamic/MapServer/29/query?where=1%3D1&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&f=json", function (obj) {       
        buildingJSON = obj.features        
        $.each(obj.features,function(key,value) {value = value.attributes
            var building = {};
            //console.log(JSON.stringify(value));
            var Alias = '',Abbr = '';
            building.name = value.Name;
            building.bldgAlpha = value.SpaceNumLetter;
            building.bldgNum = value.SpaceNumOrigin;
            building.alias = value.AliasName;
            building.abbr = value.SISAbbrev;
            building.address = value.Address;
            building.bldgUse =  value.BuildingUse;
            if (value.AliasName != null) {
                Alias = ' ' + value.AliasName;    
            }

            if(value.SISAbbrev != null) {
                Abbr = ' ' + value.SISAbbrev;
                //console.log("Abbr :: "+ Abbr);

                if (Alias == Abbr) {
                    Abbr = '';
                }
            }            
            
            buildings.push(building);
            //var buildingName = building.name
            //var isResHall = buildingName.search(" Residence Hall"); //Return results which are not residence hall
            if(building.name !== null)// && building.name.search(" Residence Hall") === -1)
            {
                buildinglist.push(building);
            }

            buildinglist.sort((a, b) => parseFloat(a.bldgAlpha) - parseFloat(b.bldgAlpha));

            //console.log("buildinglist::");
            //console.log(JSON.stringify(buildinglist));
           
        });
        //console.log(JSON.stringify(buildinglist))
    });


    function searchArray(nameKey, tpArr){
        for (var i=0; i < tpArr.length; i++) {
            if (tpArr[i].bldgNum === nameKey) {
                return tpArr[i];
            }
        }
    }
    

    function findBuildingfromDD(DDtext) {

        for (var i=0; i<buildinglist.length; i++)
            {
                if (buildings[i].DDtext == DDtext) {
                    return buildings[i].bldgAlpha;
                }
            }       
    }

    function findBuildingNofromDD(DDtext) {

        for (var i=0; i<buildings.length; i++)
            {
                
                if (buildings[i].name == DDtext) {
                    return buildings[i].bldgNum;
                }
            }       
    }
    
    function stringWrap(str){
        var str1 = str.substr(0,18)        
        var str2 = str.substr(18)
        var breakStr;

        
        var indexOfSpace = str2.indexOf(' ')
        var lastpart = str2.substr(indexOfSpace)    
        
        breakStr = str2.split(' ')[0]

        if(lastpart.length >= 19){
            lastpart = stringSecondLine(lastpart)
        }

        return [str1+breakStr,lastpart];//+'\n'+lastpart
        
    
    }

    function stringSecondLine(str) {
        var str1 = str.substr(0,18)
        var str2 = str.substr(18)
        var breakStr;
        var lastpart='';
        //console.log("str1 - str2 ::"+ str1+"-"+str2);

        var indexOfSpace = str2.indexOf(' ')
        //console.log("indexOfSpace:: "+indexOfSpace)
        if(indexOfSpace > -1){
            lastpart = str2.substr(indexOfSpace)      
        } 
        breakStr = str2.split(' ')[0]           
        
        //console.log(str1+" - "+breakStr);
        //console.log("lastpart:: "+lastpart);
        return [str1+breakStr,lastpart];//+'\n'+lastpart
    }

    function sortDropDownListByText(selected) {
        //console.log("Sort: " + selected);
        selected.html($("option", selected).sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }));

    };

    
    var buildingoptions = {
        data: buildinglist,
        getValue: function(element) {
            var stringRes='';
            if (element.abbr == null && element.alias !== null) {
                //console.log("alias"+element.alias)
                stringRes = element.name +" - "+ element.bldgAlpha +" - "+ element.alias ;
            } else if (element.abbr !== null && element.alias == null){
                //console.log("Abbr"+element.abbr)
                stringRes = element.name +" - "+ element.bldgAlpha +" - "+ element.abbr;
            } else if (element.abbr !== null && element.alias !== null){
                
                stringRes =  element.name +" - "+ element.bldgAlpha +" - "+ element.abbr +" - "+ element.alias ;
            } else {                
                stringRes =  element.name +" - "+ element.bldgAlpha;
            }

            return stringRes;
            
        },
        list: {	    maxNumberOfElements: 100,
                    match: {
                      enabled: true,
                      method:  function(element, phrase) {
                            var spacephrase = new Array(1 + 1).join(' ')+phrase ;
                            
                            if(element.indexOf(phrase) === 0) {
                                return true;
                            } else if (element.indexOf(spacephrase) > -1){
                                return true;
                            }                            
                            else {
                                return false;
                            }
                        }                                  
                    },
                    onChooseEvent: function() {
                        var DDtext = $("#BuildingDD").val().split(' - ');
                        DDtext = DDtext[0].trim();
                        selectedBuilding= findBuildingNofromDD(DDtext);
                        //console.log(selectedBuilding);
                        loadRooms(selectedBuilding);
                    }
                },  
        template: {
            type: "custom",
            method: function(value, item) {
                //console.log(item)
                //return "<img src='" + item.icon + "' /> | " + item.type + " | " + value;
                var bottomString = '';               
                
                if(item.abbr !== null && item.alias !== null){
                    //bottomString = item.abbr +" / "+ item.alias;  
                    bottomString = "<br><span title='Abbreviation'><span class=headMenu>Abbreviation: </span>"+item.abbr +"</span><br><span title='Alias'><span class=headMenu>Alias: </span>"+ item.alias+"</span>";  
                    if((item.abbr).toLowerCase() == (item.alias).toLowerCase()){ // if abbr and alias are same
                        bottomString = "<br><span title='Abbreviation'><span class=headMenu>Abbreviation: </span>"+item.abbr+ '</span>'
                    }
                } else if (item.abbr == null && item.alias !== null) {
                    //bottomString = item.alias;
                    bottomString = "<br><span title='Alias'><span class=headMenu>Alias: </span>"+ item.alias +"</span>";  
                } else if (item.abbr !== null && item.alias == null){
                    bottomString = "<br><span title='Abbreviation'><span class=headMenu>Abbreviation: </span>"+item.abbr+"</span>";
                }
                if(item.address !== null){
                    //var addressString = "Address: "+item.address// stringWrap
                    var addressString = item.address// stringWrap
                    if(addressString.charAt(0)== '')
                    {
                        addressString = addressString.substr(1);
                    }

                    /*
                    if(addressString.length > 20){
                        addressString = stringWrap(addressString);

                        if(addressString[1].indexOf(0) === ''){
                            buildingName[1].substr(1);
                        }
                        addressString[1] = "<br>"+addressString[1];
                        //console.log(addressString[1]);
                    }*/

                    bottomString = bottomString+"<br>"+addressString;
                }

                if(bottomString !== ''){
                    //bottomString = " - " +bottomString
                }
                var  strName = item.name+" ("+item.bldgAlpha+ ")";
                if(strName.length >= 19){
                    var buildingName = [];
                    var secondPart = '';
                    buildingName = stringWrap(strName);
                    //console.log("1. buildingName[0]::"+buildingName[0]);
                    //strName = strName + "Modified"

                    if(buildingName[1].length ==1){ 
                        secondPart=''
                    }
                    else if(buildingName[1].length <=3){
                        //For array with length more
                        //console.log("in the if.."+buildingName[1].length)                       

                        var bigString = buildingName[1];
                        buildingName[1]=''
                        bigString.forEach(function(element) {
                            //console.log(element);
                            secondPart = secondPart+"<br>"+element;
                        });                      
                        
                    } else {
                        //console.log("2. buildingName[1]::"+buildingName[1]);

                        if(buildingName[1].indexOf(0) === ''){
                            buildingName[1].substr(1);
                        }
                        buildingName[1] = "<br>"+buildingName[1];
                        secondPart = buildingName[1];
                        
                    }  
                   return "<span class='lineBreak'><b>"+buildingName[0].replace(/^[ ]+|[ ]+$/g,'')+secondPart.replace(/^[ ]+|[ ]+$/g,'') +"</b></span><div class=subtleGrey>" +bottomString+"</div>";
                }
                else {
                    return "<span style='word-wrap: break-word'><b>"+strName +"</b></span><div class=subtleGrey>" +bottomString+"</div>";
                }
            },
	    }                    
    };
    //console.log(buildingoptions);
    $("#BuildingDD").easyAutocomplete(buildingoptions);
    $("#BuildingDD").on('click', function() {        
        if($("#BuildingDD").val() !== ''){
            $("#BuildingDD").val('') 
            $("#roominput").val('') 
        }
    });
    
    $('.ui-autocomplete').css('max-width','25%');
