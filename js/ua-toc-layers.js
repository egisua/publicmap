function showLayer(layerArr){
    var newLayerIds = [];        
    var newLayerIds = [];
    layerArr.forEach(function(layer){
        newLayerIds = identifyParams.layerIds;
        if(newLayerIds.indexOf(layer) == -1){            
            newLayerIds.splice(0,0, layer); // Add layer at 0 index and delete 0
        }        
        identifyParams.layerIds = newLayerIds;
    });        
}

function hideLayer(layerArr){
    var newLayerIds = [];
    layerArr.forEach(function(layer){        
        newLayerIds = identifyParams.layerIds;            
        var lyrIndex =newLayerIds.indexOf(layer);            
        if(lyrIndex > -1){            
            newLayerIds.splice(lyrIndex,1);
        }        
        identifyParams.layerIds = newLayerIds;
        //console.log("identifyParams.layerIds::"+identifyParams.layerIds);
    });    
}    


function AddLayersTOC(id, callback) {
//console.log("Toc ID "+id+" clicked")
//hide layers
$.each(map.graphicsLayerIds, function (i, layer) {
    if (layer != "buildingsLayer"
    &&  layer != "UASitePoints"
    &&  layer != "construction"
    &&  layer != "greekHousesMapTips"
    &&  layer != "searchResults"
    &&  layer != "roomPolygons"
    &&  layer != "roomLabels"
    &&  layer != "customFL"
    //&&  layer != "customFL"
    ) {map.getLayer(layer).hide()};
});
$.each(map.layerIds, function (i, layer) {
    if (layer != "bing"
    &&  layer != "imagery"
    &&  layer != "ortho_tile"
    &&  layer != "enterprise_tile"
    &&  layer != "campusTilesPBC"
    &&  layer != "publicTilesCAC"
    &&  layer != "publicTilesB2"
    &&  layer != "publicTilesTechPark"
    &&  layer != "publicTilesUASouth"
    ) {map.getLayer(layer).hide()};
});
stopLiveGPSFeed();

switch (id)
{
    case "rbtAccessibility":
        $(".accessibilityTOC").toggleClass("o");
        map.getLayer("disabledParking1").show();
        map.getLayer("entrances1").show();
        hideLayer([0,8,38,53,55,56,57,62,61]);
        break;
    case "rbtAthletics":
        $(".athleticsTOC").toggleClass("o");
        map.getLayer("athletics").show();
        map.getLayer("jimClickHallFame").show();
        hideLayer([0,8,38,53,55,56,57,62,61]);
        break;
    case "rbtMuseums":
        $(".museumsTOC").toggleClass("o");
        map.getLayer("museums").show();
        hideLayer([0,8,38,53,55,56,57,62,61]);
        break;
    case "rbtSafety":
        $(".safetyTOC").toggleClass("o");
        showLayer([55,56,57]);
        map.getLayer("healthSafetyBldgs").show();
        map.getLayer("aeds1").show();
        map.getLayer("emergencyPhones1").show();
        map.getLayer("fitnessPaths").show();
        map.getLayer("safeRide").show();
        map.getLayer("allGens1").show();
        map.getLayer("womenRes1").show();
        map.getLayer("MenRes1").show();
        hideLayer([0,8,38,53,62,61]);
        break;
    case "rbtLibraries":
        $(".librariesTOC").toggleClass("o");
        map.getLayer("libraries").show();
        hideLayer([0,8,38,53,55,56,57,62,61]);
        break;
    case "rbtParking":
        $(".parkingTOC").toggleClass("o");
        map.getLayer("parkingLots").show();
        map.getLayer("parkingVisitors").show();
        map.getLayer("disabledParking1").show();
        hideLayer([0,8,38,53,55,56,57,62,61]);
        break;
    case "rbtEateries":
        showLayer([0]);
        $(".eateriesTOC").toggleClass("o");
        map.getLayer("eateries1").show();
        hideLayer([38,9,53,55,56,57,62,61]);
        break;
    case "rbtHousing":
        $(".housingTOC").toggleClass("o");            
        showLayer([0,8,38,53]);            
        //console.log("identifyParams ::"+JSON.stringify(identifyParams.layerIds));
        map.getLayer("housing_reslife").show();
        map.getLayer("housing_greek").show();
        break;
    case "rbtTransportation":
        showLayer([8]);
        $(".transportationTOC").toggleClass("o");
        map.getLayer("catTran").show();
        map.getLayer("sunLinkStreetCarStops1").show();
        map.getLayer("sunTranStops1").show();
        map.getLayer("shuttleStops").show();
        map.getLayer("bikeRoutes").show();
        getGPSFeed = true;
        getLiveGPSFeed(routeIds);
        hideLayer([0,38,53,55,56,57,62,61]);
        break;
    case "rbtAttractions":
        showLayer([62,61]);
        //console.log("RBT Attraction clickded")            
        $(".attractionsTOC").toggleClass("o");
        map.getLayer("offCampusLandmarks").show();
        map.getLayer("historicBuildingsMuseums").show();
        map.getLayer("museumsArtVenues").show();
        map.getLayer("otherAttractionsMuseums").show();
        hideLayer([0,8,38,53,55,56,57]);
        break;
    default:
        hideLayer([0,38,53]);            
}
dimTOCCheckboxes();

//executed on checkbox click event
$(document).ready(function(){
    //could replace toctop_checkbox with
    //$(".o > input").change(function() {...
    $('input.toctop_checkbox').change(function() {             
        // console.log($(this).is( ":checked" ));
        var chk = this;
        var layers = chk.value.split(",");
        $.each(layers, function(i,layer) {
            if (layer === "catTran") {
                if(chk.checked) {
                    getGPSFeed = true;
                    getLiveGPSFeed(routeIds);
                }
                else
                    stopLiveGPSFeed();
            }
            //console.log(map.getLayer(layer).visibleLayers)
        if(map.getLayer(layer).visibleLayers){ // prevents visible error for turn on feature layers on the map
            let shownLayers = map.getLayer(layer).visibleLayers;
            let arrMuesems = [61,62];
            shownLayers.forEach(function(lyrId ) {
                if(arrMuesems.indexOf(lyrId) > -1){
                    if(chk.checked){
                        identifyParams.layerIds.unshift(lyrId); // Adds the element at the start 
                    } else {
                        let indexInHideArray = identifyParams.layerIds.indexOf(lyrId);
                        if (indexInHideArray > -1) {
                            identifyParams.layerIds.splice(indexInHideArray,1); 
                            
                        }
                        
                    }
                    
                }

            });
        }
            /*
            if(chk.checked == true) {

                showLayer(map.getLayer(layer).visibleLayers);
                map.getLayer(layer).show();
            } else {
                map.getLayer(layer).hide();
                hideLayer(map.getLayer(layer).visibleLayers);
            }
            */
            chk.checked == true ? map.getLayer(layer).show() : map.getLayer(layer).hide();

        });
        //$(chk).prop("checked", !$(chk).prop("checked"));
        // if(chk.checked == true) {
        //     console.log("checked = true");
        //     $(this).removeProp("checked");
        // }
        // else
        //     $(this).prop("checked", "checked");
        // console.log(this);
        // console.log($(this).is( ":checked" ));

        //this is to hide the address bar on mobile devices.
        // window.addEventListener("load",function() {
        //     setTimeout(function(){
        //         // Hide the address bar!
        //         window.scrollTo(0, 1);
        //     }, 0);
        // });
    });

    //TODO: Not executing... Find all checkbox-status or w-icon, check to see if !$(".ico-ua-disabled-parking").hasClass("enabled")
    //if false,  $(".w-icon").addClass("enabled");
    // there are 4 exceptions though

    // if($(".o").find($(".checkbox-square.enabled > div")).hasClass("enabled") === false) {
    //     console.log("enabling");
    //     console.log($(".o").find($(".checkbox-square.enabled > div")));
    //     $(".o").find($(".checkbox-square.enabled > div")).addClass("enabled");

    // }

    // $.each($(".o").find($(".checkbox-square.enabled > div")), function (i ,icon) {
    //     if(!$(icon).hasClass("enabled")) {
    //         console.log("enabling");
    //         $(icon).addClass("enabled");
    //     }
    // });
});
//executed when clicking a Zoom button
$('.zoomToLayer').click(function() {
    switch(this.id)
    {
        case "athleticsOffCampus":
        case "rgt-athleticsOffCampus":
            xmin=-12351930;
            ymin=3790914;
            xmax=-12347209;
            ymax=3794307;
            map.setExtent(new esri.geometry.Extent(xmin,ymin,xmax,ymax, new esri.SpatialReference({ wkid:102100 })), true);
        break;
        case "athleticsOnCampus":
        case "rgt-athleticsOnCampus":
            xmin=-12351342;
            ymin=3793044;
            xmax=-12349899;
            ymax=3794081;
            map.setExtent(new esri.geometry.Extent(xmin,ymin,xmax,ymax, new esri.SpatialReference({ wkid:102100 })));
            break;
        case "museums":
        case "rgt-museums":
            xmin=-12351802;
            ymin=3793531;
            xmax=-12349998;
            ymax=3794360;
            map.setExtent(new esri.geometry.Extent(xmin,ymin,xmax,ymax, new esri.SpatialReference({ wkid:102100 })));
            break;
        case "police":
        case "rgt-police":
            map.centerAndZoom(new esri.geometry.Point(-12350256, 3794145, new esri.SpatialReference({ wkid: 102100 })), 19);
            break;
        case "campusRec":
        case "rgt-campusRec":
            map.centerAndZoom(new esri.geometry.Point(-12350864, 3793162, new esri.SpatialReference({ wkid: 102100 })), 18);
            break;
        case "hospital":
        case "rgt-hospital":
            map.centerAndZoom(new esri.geometry.Point(-12350368, 3795001, new esri.SpatialReference({ wkid: 102100 })), 18);
            break;
        case "campusHealth":
        case "rgt-campusHealth":
            map.centerAndZoom(new esri.geometry.Point(-12351111, 3793293, new esri.SpatialReference({ wkid: 102100 })), 19);
            break;

        case "safeRide":
        case "rgt-safeRide":
            map.centerAndZoom(new esri.geometry.Point(-12351020, 3794292, new esri.SpatialReference({ wkid: 102100 })), 15);
            break;
        case "libraries":
        case "rgt-libraries":
            xmin=-12351681;
            ymin=3793512;
            xmax=-12350380;
            ymax=3794652;
            map.setExtent(new esri.geometry.Extent(xmin,ymin,xmax,ymax, new esri.SpatialReference({ wkid:102100 })), true);
            break;
        case "eateriesSUMC":
            map.centerAndZoom(new esri.geometry.Point(-12351123, 3793875, new esri.SpatialReference({ wkid: 102100 })), 19);
            break;
        case "eateriesPSU":
            map.centerAndZoom(new esri.geometry.Point(-12351715, 3793459, new esri.SpatialReference({ wkid: 102100 })), 19);
            break;
        case "eateriesAll":
        case "rgt-eateries3":
            xmin=-12352227;
            ymin=3792975;
            xmax=-12350493;
            ymax=3794721;
            map.setExtent(new esri.geometry.Extent(xmin,ymin,xmax,ymax, new esri.SpatialReference({ wkid:102100 })));
            break;
        case "oldMain":
        case "rgt-oldMain":
            map.centerAndZoom(new esri.geometry.Point(-12351277, 3793766, new esri.SpatialReference({ wkid: 102100 })), 19);
            break;
        case "visitorCenter":
        case "rgt-visitorCenter":
            map.centerAndZoom(new esri.geometry.Point(-12351989, 3793799, new esri.SpatialReference({ wkid: 102100 })), 19);
            break;
        case "offCampusLandmarks":
        case "rgt-offCampusLandmarks":
            xmin=-12409320;
            ymin=3754020;
            xmax=-12262561;
            ymax=3977524;
            map.setExtent(new esri.geometry.Extent(xmin,ymin,xmax,ymax, new esri.SpatialReference({ wkid:102100 })));
            break;
        default:
            console.log("fell through switch statement");
            map.setExtent(map.getLayer(this.id).fullExtent);
    }
 });
 callback();
}

function showTOCLayers(_div) {
console.log("showTOCLayers(_div) has been deprecated");
//could use... $.each($(".o > input"), function (i, chk) {
// but I think I need to keep the function parameter (_div).
/*
$.each($("." + _div).children("input"), function (i, chk) {
    console.log("Should checkbox  " + chk.id + " be checked? " + chk.checked);
    var layers = chk.value.split(",");
    $.each(layers, function(i,layer) {
        console.log("turning on: " + layer);
        map.getLayer(layer).show();
        console.log(layer + " is now on");
    });
});
console.log("END showTOCLayers");
*/
}

function dimTOCCheckboxes() {  
$(".o > input").prop("disabled", true);
try {        
    if(map.getLayersVisibleAtScale){
        $.each(map.getLayersVisibleAtScale(map.getScale()), function (i, layer) {
            $.each($(".o > input"), function (i, chk) {
                //console.log("Layer ID:: "+layer.id);
                if (chk.value.search(layer.id) >= 0) {
                    chk.disabled = false;
                }
                $("#" + chk.id + "+ label").toggleClass("zoomDisabled", chk.disabled);
            });
        });
    }    
} catch(err)
{
    console.log(err);
}

}

//LET'S KEEP THIS
/*
function dimTOCCheckboxesBak() {
$("input.toctop_checkbox").prop("disabled", true);

$.each(map.getLayersVisibleAtScale(map.getScale()), function (i, layer) {
    //console.debug("checking " + layer.id);
    $.each($("input.toctop_checkbox"), function (i, chk) {
        //console.debug("      CHECKBOX VALUE:" + chk.value);
        if (chk.value.search(layer.id) >= 0) {
            //console.debug("MATCH! - " + layer.id);
            console.log("checkbox id: " + chk.id);
            chk.disabled = false;
        }
    });
});
}
*/