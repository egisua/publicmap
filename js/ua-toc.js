var tocTopTitle = "";
var gerry = 0;


$('.roomBanner').click(function(){
  if(document.getElementsByClassName('visible').length > 0)
  {
    $('.right-side-tools').toggleClass('visible', '1000');
  }  
  $('#tab2').click();
});

$(window).load(function (){
  $('#right-side-tool-tab').click(function (){
    $('.right-side-tools').toggleClass('visible', '1000');
    $('.claro').removeClass('more-info', '1000');
    $('.right-side-tools').removeClass('more-info', '1000');
    return false;
  });

  /*
  $("input[type='radio']").change(function (){
    changeLayers(this);
  });
  

  $("input[type='radio']").click(function (){
    console.log("Radio clicked..")
    changeLayers(this);
  });*/
  
  $('.my-location-wrapper').click(function(){
    $('.my-location').toggleClass('on', '500');
    return false;
  });

  $('.layer-controls-toggle').click(function(){
    $('.layer-controls-wrapper').toggleClass('up');
    return false;
  });
  $('input:radio').change(function() {
    var f = $(this).attr('id');
    $('.button-tag').removeClass('checked');
    $('label[for='+f+']').addClass('checked');
  });
  $('input:checkbox').change(function() {
    var f = $(this).attr('id');
    // $('.checkbox-square').removeClass('ico-ua-x');
    $('label[for='+f+'] .checkbox-status.no-icon').toggleClass('ico-ua-x');
  });
  $('input:checkbox').change(function() {
    var f = $(this).attr('id');
    // $('.checkbox-square').removeClass('ico-ua-x');
    $('label[for='+f+'] .w-icon').toggleClass('enabled');
  });
  $('.share-overlay').click(function(){
    $('.share-overlay').toggleClass('on');
  });

  $('.share-overlay').click(function(){
    $('.share-overlay').removeClass('on');
    $('#share-link').removeClass('on');
  });

  $('.esriSimpleSliderIncrementButton').attr('data-toggle','tooltip').attr('data-original-title', 'Zoom In').attr('data-placement','right').tooltip();
  $('.esriSimpleSliderDecrementButton').attr('id', 'tour4').attr('data-toggle','tooltip').attr('data-original-title', 'Zoom Out').attr('data-placement','right').tooltip();
  $('.esriSimpleSliderReload').attr('data-toggle','tooltip').attr('data-original-title', 'Reset Map').attr('data-placement','right').tooltip();
  $('.ico-ua_satellite').attr('data-toggle','tooltip').attr('data-original-title', 'Imagery').attr('data-placement','right').tooltip();
  $('.ico-ua-360').attr('data-toggle','tooltip').attr('data-original-title', 'Aerial 360').attr('data-placement','right').tooltip();
  
  $('.ico-ua_social').tooltip();
  $('.ico-ua_info').tooltip();
  $('.ico-ua-print').tooltip();

  /*
  var view = new ReloadView($('#tourReload'));
  //console.log($('#tourReload'));
  var steps = [{
    content: '<div class="tour-brand"></div>Welcome to the<br><strong>University of Arizona<br> Interactive Map</strong>',
    highlightTarget: false,
    nextButton: true,
    closeButton: true,
    target: $('#tourstart'),
    my: 'right bottom',
    at: 'left center'
  },  {
    content: '<h2>Reset Map</h2><p>You can reset the map to the initial state by clicking the <i class="ico-reload"></i> icon</p>',
    highlightTarget: true,
    closeButton: true,
    nextButton: true,
    target: $('#tourReload'),
    my: 'left center',
    at: 'right center',
    setup: function(tour, options){
     $('#tourReload').bind('click.stepTwo',function(){
      tour.next();
    })
   },
   teardown: function(tour, options){
     $('#tourReload').unbind('click.stepTwo');
     $('#tab2').click()
   },
 },
 
{
  content: '<h2>Search Box</h2><p>Type building name into the search box.</p><p><b>Note: </b>Building number, alias or abbreviation will work equally.</p>',
  highlightTarget: true,
  nextButton: true,
  closeButton: true,
  target: $('#BuildingDD'),
  my: 'right center',
  at: 'left center',
  setup: function(tour, options){
  
   $('#BuildingDD').bind('click.stepThree', function(event){
      tour.next();
    })
 },
 teardown: function(tour, options){
   $('#BuildingDD').unbind('click.stepThree');
   $('#tab1').click();
 },
},

{
  content: '<h2>INTERACTIVE LAYERS</h2><p>Click on the parking layer to move to the next step in the tour.</p><p>You will then see parking lots appear on the  map.</p>',
  highlightTarget: true,
  closeButton: true,
  target: $('#tour2'),
  my: 'right center',
  at: 'left center',
  setup: function(tour, options){
   $('#tour2').bind('click.stepFour',function(){
    setTimeout(function(){tour.next()},900);
    
  })
 },
 teardown: function(tour, options){
   $('#tour2').unbind('click.stepFour');
 },
},{
  content: '<h2>Layer Controls</h2><p>A key will drop down to show related information which you are able to turn on and off.</p><p>Try clicking the word "bicycle" to the right of the <span class="checkbox-status w-icon ico-ua-bicycle-parking"></span>icon above</p>',
  highlightTarget: true,
  nextButton: true,
  closeButton: true,
  target: $('#tour3'),
  my: 'top center',
  at: 'bottom center',
  setup: function(tour, options){
   $('#tour3').bind('click.stepFive',function(){
    tour.next();
  })
 },
 teardown: function(tour, options){
   $('#tour3').unbind('click.stepFive');
 },
},{
  content: '<h2>ZOOM CONTROL</h2><p>You can zoom out to see this building in relation to the rest of campus.</p>',
  highlightTarget: true,
  nextButton: true,
  closeButton: true,
  target: $('.esriSimpleSliderDecrementButton'),
  my: 'left center',
  at: 'right center',
  setup: function(tour, options){
   $('.esriSimpleSliderDecrementButton').bind('click.stepSix',function(){
    tour.next();
  })
 },
 teardown: function(tour, options){
   $('.esriSimpleSliderDecrementButtontour4').unbind('click.stepSix');
 },
},{
  content: '<h2>Share</h2><p>You can then share the current state of the map by clicking the &nbsp;<span class="ico-ua_social"></span>&nbsp; icon</p>',
  highlightTarget: true,
  nextButton: true,
  closeButton: true,
  target: $('#link'),
  my: 'right bottom',
  at: 'left bottom'
}
 ,{
  content: '<h2>More Resources</h2><p>Click "MORE RESOURCES" to reveal links to other maps and directions"</p>',
  highlightTarget: true,
  nextButton: true,
  closeButton: true,
  target: $('#tour5'),
  my: 'right center',
  at: 'left center',
  setup: function(tour, options){
    $('#tour5').focus().bind('change.stepThree', function(event){
      tour.next();
    })
  },
  teardown: function(tour, options){
   $('#tour5').focus().unbind('change.stepThree');
 }
}
]

  tour = new Tourist.Tour({
    steps: steps,
    tipClass: 'Bootstrap',
    tipOptions:{ showEffect: 'slidein' }
  });

  //$('.tour-link').click(function(){ tour.start(); });



function ReloadView(el){
  var self = this;
  var reload = el.find('i');

  reload.click(function(e){
    if(!el.hasClass('disabled'))
      self.select($(e.target))
  });

  this.disable = function(){
    el.addClass('disabled');
  }
  this.enable = function(){
    el.removeClass('disabled');
  }
  this.reset = function(){
    el.removeClass('has-selection');
    el.find('a').removeClass('selected');
  }
  this.select = function(reload){
    reload.removeClass('selected')
    reload.addClass('selected')
    el.addClass('has-selection')
    this.trigger('select', this, reload);
  }
  this.getSelected = function(){
    return el.find('.selected');
  }
}
_.extend(ReloadView.prototype, Backbone.Events)

$(function(){
  $('.tour-link').click(function(){ tour.start(); });
  return false;
});

*/
jQuery(window).ready(function(){
  jQuery("#btnInit").click(initiate_watchlocation);
  jQuery("#btnStop").click(stop_watchlocation);
});
var watchProcess = null;
function initiate_watchlocation() {
}
function stop_watchlocation() {
}
function handle_errors(error)
{
 switch(error.code)
 {
  case error.PERMISSION_DENIED: alert("user did not share geolocation data");
  break;
  case error.POSITION_UNAVAILABLE: alert("could not detect current position");
  break;
  case error.TIMEOUT: alert("retrieving position timedout");
  break;
  default: alert("unknown error");
  break;
}
}
function handle_geolocation_query(position) {
}
$(".loc-off").click(function() {
  if(gerry === 0){

    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition(zoomToLocation, locationError);
      watchId = navigator.geolocation.watchPosition(showLocation, locationError);
      $(this).removeClass('loc-off').addClass('loc-on');
      $('.loc-on').click(function(){
        stopWatching('');
      });
    }
    else {
      alert("Browser doesn't support Geolocation. Visit http://caniuse.com to discover browser support for the Geolocation API.");
    }
    gerry = 1;
  }
  else {
    navigator.geolocation.clearWatch(watchId);
    $('map_graphics_layer').remove('.map_graphics_layer');

    gerry = 0;
  }
});
});

function stopWatching(){
 $(".my-location-wrapper").removeClass('loc-on', '1000');
 $(".my-location-wrapper").addClass('loc-off', '1000');
      navigator.geolocation.clearWatch();  //navigator.geolocation.stop_watchPosition();
      $('#map_graphics_layer').remove();
      alert("Babagaluh");
    }

    function getMoreResources() {
      $('.moreInfo').removeClass('show');
      $('.moreInfo').addClass('show');
      $('.claro').toggleClass('more-info', '1000');
      $('.right-side-tools').toggleClass('more-info', '1000');

      if ($(".layer-controls-wrapper").hasClass("on")){
        $(".layer-controls-wrapper").toggleClass("on");
      }
    }
    function getCandyBar() {
      document.getElementById("candyBar").innerHTML = "<a href=\"#\" alt=\"Take the Tour\" class=\"tour-link\" data-tour=\"tour1\" id=\"tourstart\"><div>Map Help<\/div><\/a>\r\n\r\n<a href=\"#\" name=\"link\" class=\"link\" id=\"link\" onclick=\"getLink()\" alt=\"Share\">\r\n<div class=\"arizona-btn\">\r\n<div class=\"fs1 ico-ua_social\" aria-hidden=\"true\" data-ico=\"\uE000\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Share\"><\/div>\r\n<\/div>\r\n<\/a>\r\n\r\n<a href=\"http:\/\/www.arizona.edu\/website\/map-feedback\" name=\"help\" class=\"help\" alt=\"Send Feedback\" target=\"_blank\">\r\n<div class=\"arizona-btn\">\r\n<div class=\"glyph\">\r\n<div class=\"fs1 ico-ua_info\" aria-hidden=\"true\" data-ico=\"\uE000\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Send Feedback\"><\/div>\r\n<\/div>\r\n<\/div>\r\n<\/a>\r\n\r\n<a href=\"#\" name=\"print\" class=\"print\" id=\"print\" onclick=\"getPrint()\" alt=\"Print\">\r\n<div class=\"arizona-btn\">\r\n<div class=\"glyph\">\r\n<div class=\"fs1 ico-ua-print\" aria-hidden=\"true\" data-ico=\"\uE000\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Print\"><\/div>\r\n<\/div>\r\n<\/div>\r\n<\/a>";
      $('.ico-ua_social').tooltip();
      $('.ico-ua_info').tooltip();
      $('.ico-ua-print').tooltip();
    }
    function getLink() {
      $('.share-overlay').addClass('on');
      $('#share-link').addClass('on');
      document.getElementById("share-link").innerHTML = "<div class=\"social-network\"><a id=\"facebook\" class=\"social-link\" target=\"_blank\"><div class=\"social-icon ss-facebook\"></div>Share on Facebook</a><a id=\"twitter\" class=\"social-link\" target=\"_blank\"><div class=\"social-icon ss-twitter\"></div> Share on Twitter</a><a id=\"email\" class=\"social-link\" target=\"_blank\"><div class=\"social-icon ss-mail\"></div>Share via Email</a><a id=\"gplus\" class=\"social-link\" target=\"_blank\"><div class=\"social-icon ss-googleplus\"></div>Share on Google+</a></div><p id=\"copy-button\" data-clipboard-target=\"#linkBox\">Click to Copy the Link</p><input type=\"text\" class=\"arizona-text-box link\" id=\"linkBox\" value=\"http://map.arizona.edu\">";
      var qs = "http://" + window.location.hostname.toString() + window.location.pathname.toString() + "?" +"x=" + map.extent.getCenter().x.toFixed(0) +"&y=" + map.extent.getCenter().y.toFixed(0) +"&lod=" + map.getZoom();

      if($('input[name=layer]:checked').length > 0) {
        qs = qs + "&toc=" + $("input[name=layer]:checked").attr("id");
      }

    // if($("#searchbox").val() !== '') {
    //   qs = qs + "&id=" + $("#searchbox").val();
    // }
    if (map.infoWindow.isShowing === true) {
      console.log("Info win showing")
      //qs = qs + "&id=" + map.getLayer("searchResults").graphics[0].attributes.BID;
      //console.log("Examine:: "+map.getLayer("searchResults").graphics[0].results[0].feature.attributes);
      //qs = qs + "&id=" + map.getLayer("searchResults").graphics[0].attributes.EGISID;
      //console.log(map.getLayer("searchResults"));
      console.log(selectedGraphics)
      qs = qs + "&id=" + selectedGraphics; //map.getLayer("searchResults").graphics[0].results[0].feature.attributes.EGISID;
    }
    console.log("Query String :"+qs);   

    var link = qs;
    $('#linkBox').val(link);
    $('#facebook').attr('href', 'https://www.facebook.com/sharer/sharer.php?u=' + link);
    $('#gplus').attr('href', 'https://plus.google.com/share?url=' + link);
    $('#email').attr('href', 'mailto:?subject=Campus%20Map%20-%20University%20of%20Arizona&body=' + encodeURIComponent(link));
    $('#twitter').attr('href', 'https://twitter.com/intent/tweet?url=' + encodeURIComponent(link) + '&via=uofa');
    $('#copy-button').attr('data-clipboard-text', link).attr('title','Link copied to clipboard').attr('data-toggle','tooltip').attr('data-placement','top').attr('data-original-title','Copied text to clipboard' + link).attr('data-trigger','manual').tooltip();

/*
//TODO: Chris, the uanow.org shortener kept resolving to http://arizona.edu. I went back to goo.gl. Comment/Uncomment lines 287,288 and 296,297
    $.ajax({
        url: 'https://www.googleapis.com/urlshortener/v1/url?shortUrl=http://goo.gl/fbsS&key=AIzaSyBR2DDBkh4TQxsD4jKEqCOOEAH-_tf-5qE',
        //url: 'https://www.googleapis.com/urlshortener/v1/url?shortUrl=http://goo.gl/fbsS&key=AIzaSyD7ECdQmnJX1zTdosc1KfB46C3L24MMNq0',
        //url: 'http://uanow.org/shurly/api/shorten?longUrl=http://www.arizona.edu&apiKey=6d9b25d338e5ba7d8520474ff4acd33a_A',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: '{ longUrl: "' + qs +'#&utm_source=map-social-link-url&utm_medium=share-link&utm_campaign=from-share-links"}',
        dataType: 'json',

        success: function(data) {
          //UANOW shortener expects data.shortUrl. Goo.gl shortner expects data.id on success.
          // var link = data.shortUrl;
          var link = data.id;
          $('#linkBox').val(link);
          $('#facebook').attr('href', 'https://www.facebook.com/sharer/sharer.php?u=' + link);
          $('#gplus').attr('href', 'https://plus.google.com/share?url=' + link);
          $('#email').attr('href', 'mailto:?subject=Campus%20Map%20-%20University%20of%20Arizona&body=' + encodeURIComponent(link));
          $('#twitter').attr('href', 'https://twitter.com/intent/tweet?url=' + encodeURIComponent(link) + '&via=uofa');
          $('#copy-button').attr('data-clipboard-text', link).attr('title','Link copied to clipboard').attr('data-toggle','tooltip').attr('data-placement','top').attr('data-original-title','Copied text to clipboard' + link).attr('data-trigger','manual').tooltip();
       }
     });
  */
     
/*  
var motivatebox = document.getElementById("copy-button");
motivatebox.addEventListener('mouseup', function(e){
  console.log("Mouse Up");
});
*/
  copyToClipboard('copy-button');


/*   
var clip = new ZeroClipboard( document.getElementById("copy-button"), {
  //moviePath: "zeroclipboard/ZeroClipboard.swf"
} );
var clip =  document.getElementById("copy-button");

var clip = ClipboardJS("copy-button");

clip.on( 'load', function(client) {
      // alert( "movie is loaded" );
    } );

clip.on( 'complete', function(client, args) {
  $('#copy-button').tooltip('show');
} );

clip.on( 'mouseover', function(client) {
      // alert("mouse over");
    } );

clip.on( 'mouseout', function(client) {
      // alert("mouse out");
    } );

clip.on( 'mousedown', function(client) {

      // alert("mouse down");
    } );

clip.on( 'mouseup', function(client) {
      // alert("mouse up");
    } );
*/

} //-- End of getLink() Function


function copyToClipboard(copyButtonId){
 
  var clipboard = new ClipboardJS('#'+copyButtonId);
    clipboard.on('success', function(e) {
    console.info('Action:', e.action);
    console.info('Text:', e.text);
    console.info('Trigger:', e.trigger);
    });

}
/**/

function getPrint() {
  document.getElementById("candyBar").innerHTML = "<div class=\"arizona-btn brillant candy\"><div class=\"candy-close\" onclick=\"getCandyBar()\"></div>";
//parameter is the title of the printed map.
createPrintDijit("University of Arizona Map");
}

function toc_init() {     
           
      
      //$.getJSON("http://services.maps.arizona.edu/pdc/rest/services/publicDynamic/MapServer/29/query?where=v_public_buildings.BL_NAME_PUBLIC+<>+%27%27+AND+v_public_buildings.BL_NAME_PUBLIC+<>+%27Leased+Out%27++AND+v_public_buildings.BL_NAME_PUBLIC+<>+%27TBA%27&returnGeometry=false&outFields=Buildings.OBJECTID%2C+v_public_buildings.BL_NAME_PUBLIC&orderByFields=v_public_buildings.BL_NAME_PUBLIC&f=json",  function(data) {
      //using Buildings Map Service
      //http://services.maps.arizona.edu/pdc/rest/services/publicDynamic/MapServer/29/query?where=Name+%3C%3E+%27%27+AND+Name+%3C%3E+%27Leased+Out%27+AND+Name+%3C%3E+%27TBA%27+AND+BID+is+not+null&returnGeometry=false&outFields=BID%2C+Name%2C+AliasName&orderByFields=Name&f=json
      //http://pdc-betagis2:6080/arcgis/rest/services/publicDynamic/MapServer/29/query?where=Name+%3C%3E+%27%27+AND+Name+%3C%3E+%27Leased+Out%27+AND+Name+%3C%3E+%27TBA%27+AND+BID+is+not+null&returnGeometry=false&outFields=BID%2C+Name%2C+AliasName&orderByFields=Name&f=json
          //check for parameters in the URL. If no results are found return null, else return results or 0.
          $.urlParam = function(name) {
            console.log(name)           
            
            //console.log(window.location.search);
            
            var results = new RegExp('[?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (!results) {
              return null;
            }
            return results[1] || 0;
          };


             

          /*
          $.getJSON("json/buildings.json",  function(data) {
            var txt;
            for (i in data["features"]) {
              var aliasName = data["features"][i]["attributes"]["AliasName"];
              if (aliasName == ' ' || aliasName == '' || aliasName == null)
                txt = data["features"][i]["attributes"]["Name"] + " - ( " + data["features"][i]["attributes"]["SpaceNumLetter"] + " )"
              else
                txt = data["features"][i]["attributes"]["Name"] + " - " + data["features"][i]["attributes"]["AliasName"] + " ( " + data["features"][i]["attributes"]["SpaceNumLetter"] + " )" ;
              $('#searchbox')
              .append($("<option></option>")
                //.attr("value",data["features"][i]["attributes"]["BID"])
                .attr("value",data["features"][i]["attributes"]["EGISID"])
                .text(txt)
                );
            }
            $("#searchbox").chosen({allow_single_deselect:true});
            $("#searchbox").chosen().change(doSearch);
          });
          */


          $('#chkImagery').change(function() {
            if(this.checked) {
              map.getLayer("imagery").show();
              map.getLayer("ortho_tile").show();
              map.getLayer("bing").hide();
              map.getLayer("enterprise_tile").hide();
              map.getLayer("campusTilesPBC").hide();
              map.getLayer("publicTilesCAC").hide();
              map.getLayer("publicTilesB2").hide();
              map.getLayer("publicTilesTechPark").hide();
            }
            else {
              map.getLayer("imagery").hide();
              map.getLayer("ortho_tile").hide();
              map.getLayer("bing").show();
              map.getLayer("enterprise_tile").show();
              map.getLayer("campusTilesPBC").show();		
              map.getLayer("publicTilesCAC").show();
              map.getLayer("publicTilesB2").show();
              map.getLayer("publicTilesTechPark").show();		  
            }
          });


          $('#chkAerial360').change(function() {
            if(this.checked) {
              //console.log("this is checked..");
              map.getLayer("aerial361").show();
             
              map.getLayer("aerial360").show();
              var newLayerIds = identifyParams.layerIds;
              if(newLayerIds.indexOf(59) == -1){            
                newLayerIds.splice(0,0, 59); // Add layer at 0 index and delete 0
               }  
               identifyParams.layerIds = newLayerIds;
              console.log(newLayerIds);
              /* */
              /*
              map.getLayer("imagery").show();
              map.getLayer("bing").hide();
              map.getLayer("enterprise_tile").hide();
              map.getLayer("campusTilesPBC").hide();
              map.getLayer("publicTilesCAC").hide();
              map.getLayer("publicTilesB2").hide();
              map.getLayer("publicTilesTechPark").hide();              			  
              */
            }
            else {
              console.log("this is un--checked..");
              map.getLayer("aerial361").hide();
              
              var newLayerIds = identifyParams.layerIds;            
              var lyrIndex = newLayerIds.indexOf(59);            
              if(lyrIndex > -1){            
                  newLayerIds.splice(lyrIndex,1)
              }        
              identifyParams.layerIds = newLayerIds;
              /**/
            }
          });
        }

        /*
        $(window).load(function() {
          if($.urlParam('id') != null) {
            $('#searchbox').val($.urlParam('id'));
            $('#searchbox').trigger("liszt:updated");
            doSearch();
          }
        });
        */


        function changeLayers(_radio) {
          //console.log("Change Layers...!");
          if ($(".layer-controls-wrapper").hasClass("on")){
            $(".layer-controls-wrapper").toggleClass("on");

            setTimeout(function(){
              if (tocTopTitle == "") {
                $("div").removeClass("o");
                
                AddLayersTOC(_radio.id,function(res){
                  console.log('add toc layer...2');
                });
              }
              else {
                $("div").removeClass("o");
                tocTopTitle = _radio.value;
              }
            },200);

            setTimeout(function(){
              $(".layer-controls-wrapper").toggleClass("on")
              if ($(".layer-controls-wrapper").hasClass("up")){
                $(".layer-controls-wrapper").removeClass("up");
              }
            },300);
          }
          else {
           // triggered through share link 
           setTimeout(function(){
            if (tocTopTitle == "") {
              $("div").removeClass("o");
              
              AddLayersTOC(_radio.id,function(res){
               console.log('add toc layer...3');
               
               if($.urlParam('id') != null) {
                 let EGISID = $.urlParam('id');
                 addEGISISFeature(EGISID);                 
                }
                
              });
            }
            else {
              $("div").removeClass("o");
              tocTopTitle = _radio.value;
            }
          },200);
           setTimeout(function(){
            $(".layer-controls-wrapper").toggleClass("on")
          },300);
         };
       }


    function addEGISISFeature(EGISID){

      console.log("identifyParams.layerIds: "+identifyParams.layerIds);
      let find = new esri.tasks.FindTask("https://services.maps.arizona.edu/pdc/rest/services/publicDynamic/MapServer");
      let findParams = new esri.tasks.FindParameters();
      findParams.layerIds = identifyParams.layerIds;
      findParams.returnGeometry = true;
      findParams.searchFields = ["EGISID"];
      findParams.searchText = EGISID;
      find.execute(findParams, function(res){
        console.log('search res:', res, EGISID); 
        let graphicGeom = res[0].feature.geometry;
        let pointToClick;
        if(graphicGeom['x']) { // feature is line
          pointToClick = graphicGeom;
        } else { //feature is polygon
            pointToClick = graphicGeom.getExtent().getCenter();
        }
        
        //var currentMapCenter = map.getExtent().getCenter();
        console.log(pointToClick);

        //let graphic = res[0].feature.geometry;
        map.emit('click', { mapPoint: pointToClick });
        //res[0].feature.geometry.click();
      });

      /*
        $('#searchbox').val($.urlParam('id'));
        $('#searchbox').trigger("liszt:updated");
        doSearch();
      */

    }

    function locationError(error) {
      //error occurred so stop watchPosition
      if(navigator.geolocation){
        navigator.geolocation.clearWatch(watchId);
      }
      switch (error.code) {
        case error.PERMISSION_DENIED:
        alert("Location not provided");
        break;

        case error.POSITION_UNAVAILABLE:
        alert("Current location not available");
        break;

        case error.TIMEOUT:
        alert("Timeout");
        break;

        default:
        alert("unknown error");
        break;
      }
  }

  function zoomToLocation(location) {
    var pt = esri.geometry.geographicToWebMercator(new esri.geometry.Point(location.coords.longitude, location.coords.latitude));
    addGraphic(pt);
    map.centerAndZoom(pt, 20);
  }

  function showLocation(location) {
    //zoom to the users location and add a graphic
    var pt = esri.geometry.geographicToWebMercator(new esri.geometry.Point(location.coords.longitude, location.coords.latitude));
    if (!graphic) {
      addGraphic(pt);
    }
    else { //move the graphic if it already exists
      graphic.setGeometry(pt);
    }
    map.centerAt(pt);
  }

  function addGraphic(pt){
    var symbol = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE, 12,
      new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID,
        new dojo.Color([147, 164, 69, .3]), 8),
      new dojo.Color([147, 164, 69])
      );
    graphic = new esri.Graphic(pt, symbol);
    map.graphics.add(graphic);
  }