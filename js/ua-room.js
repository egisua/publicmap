require([
    "esri/map", "esri/tasks/query", "esri/tasks/QueryTask", "esri/tasks/GeometryService",
    "esri/layers/GraphicsLayer",
    "esri/layers/FeatureLayer",
    "esri/layers/ArcGISTiledMapServiceLayer",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/graphic",
    "esri/graphicsUtils",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/Color",
    "dojo/domReady!"
],
function (Map, Query, QueryTask, GeometryService,
    GraphicsLayer,
    FeatureLayer,
    ArcGISTiledMapServiceLayer,
    ArcGISDynamicMapServiceLayer,
    Graphic,
    graphicsUtils,
    SimpleLineSymbol,
    SimpleFillSymbol,
    Color) {


// Decode URL Parameters
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

//Room parameters
var roomParameter = getUrlParameter('room');
var bldgParameter = getUrlParameter('bldg');
var bldgAlphaParameter = getUrlParameter('bldg_alpha');

// Establish "Find Room" Query Expression based on URL Parameters
var queryExpression;
if (bldgAlphaParameter != undefined)      { queryExpression = "ROOMEXT.BldgAlpha = '" + bldgAlphaParameter + "' AND ROOMEXT.RM_ID = '" + roomParameter + "'"; }
if (bldgParameter != undefined) { queryExpression = "ROOMEXT.BL_ID = '" + bldgParameter + "' AND ROOMEXT.RM_ID = '" + roomParameter + "'"; }

var interiorServiceURL = "https://services.maps.arizona.edu/pdc/rest/services/Interior/MapServer/";

var interiorMapping = new ArcGISDynamicMapServiceLayer(interiorServiceURL, {
    id: "interiorMapping",
    opacity: 0.8,
    visible: "false"
});

// For all layers in Interior Mapping, create a feature layer
// if the layer starts with "Level" (Polygonal Room Features), pass fc to setupQuery to see if our room can be found.
interiorMapping.on("load", function () {
    var layerInfos = map.getLayer("interiorMapping").layerInfos;
    for (i = 0; i < layerInfos.length; i++) {
        var fc;

        fc = new FeatureLayer(interiorServiceURL + i, { "opacity": 0.0 }, {
            id: layerInfos[i].name,
            mode: FeatureLayer.MODE_ONDEMAND,
            visible: layerInfos[i].defaultVisibility,
            outFields: ["*"]
        });

        // If this layer starts with "Level", it has polygons we want to search.  Send FC to setupQuery(fc)
        if (layerInfos[i].name.substring(0, 5) == "Level") {
            setupQuery(fc);
        }
    }
});

// Add Dynamic Layers to Map
map.addLayer(interiorMapping);

function loadInterior(){
    console.log("Load interior....!")
}

// This function will perform a query, if found, create a highlighted room feature to add to the map.
// Sets layer visibility to show the room polygons and line floorplan appropriate for the queried room.
// Sets the maps extent/zoom to show the building the room is found in.
function setupQuery(fc) {
    console.log("Q T E");
    var queryTask = new QueryTask(fc.url);

    //build query filter
    var query = new Query();
    query.returnGeometry = true;
    query.where = queryExpression;

    queryTask.on("complete", function (event) {

        var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                                            new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0, 1]), 5),
                                            new Color([255, 0, 0, .25]));

        var features = event.featureSet.features;

        var queryGraphicsLayer = new GraphicsLayer();

        // If this query has found our room feature, then make this the visible level
        // Create the graphic from the feature, add it to the GraphicsLayer, and add that to the map.
        if (features.length > 0) {
            var currentFloor = fc.name.split("_")[1]  // the Layer name ends in the current floor. "Level_10"
            updateLayerVisibility(currentFloor)

            //QueryTask returns a featureSet.
            //Loop through features in the featureSet and add them to the map.
            var featureCount = features.length;

            for (var i = 0; i < featureCount; i++) {
                //console.log(fc.name);

                // Create the graphic from the feature, add it to the GraphicsLayer, and add that to the map.
                var graphic = features[i]; //Feature is a graphic
                graphic.setSymbol(symbol);
                queryGraphicsLayer.add(graphic);
            }

            // Add the results of the query - A highlighted room polygon.
            map.addLayer(queryGraphicsLayer);

            // Set zoom/extent to show whole building this room is in.
            zoomToBuilding(fc);
            //map.setExtent(esri.graphicsExtent(features).expand(16));
        };

    });

    queryTask.execute(query);
}

// This function does a new query to get all features of a building and use that to set the zoom and extent
// This provided better results compared to setting zoom/extent based on the selected room.
function zoomToBuilding(fc) {
    var queryTask = new QueryTask(fc.url);

    var query = new Query();
    query.returnGeometry = true;
    if (bldgAlphaParameter != undefined) { query.where = "ROOMEXT.BldgAlpha = '" + bldgAlphaParameter + "'"; }
    else { query.where = "ROOMEXT.BL_ID = '" + bldgParameter + "'"; }

    queryTask.on("complete", function (event) {
        var features = event.featureSet.features;
        var queryGraphicsLayer = new GraphicsLayer();
        map.setExtent(esri.graphicsExtent(features).expand(1.4));
    });

    queryTask.execute(query)
}


// This function will make sure that both the polygons and the floorplan lines are shown for the "currentFloor"
function updateLayerVisibility(currentFloor) {
    var visibleLayerIds = [];
    //console.log("Current Floor: " + currentFloor);
    var layerInfos = map.getLayer("interiorMapping").layerInfos;
    for (i = 0; i < layerInfos.length; i++) {
        //console.log(layerInfos[i].name);
        // This will check the last one or two characters to see if they match B1, B2, G, 1, etc.  It uses the length of currentFloor
        if (layerInfos[i].name.substr(layerInfos[i].name.length - currentFloor.length) == currentFloor) {
            //console.log("Show: " + layerInfos[i].name)
            visibleLayerIds.push(i)
        }
    }

    //console.log(visibleLayerIds);
    interiorMapping.setVisibleLayers(visibleLayerIds);

}

});